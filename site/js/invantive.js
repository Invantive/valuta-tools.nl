$invantive = function() {}

$invantive.configureAngular = function(app)
{
         app.config(['$locationProvider', function($locationProvider) { 
            $locationProvider.html5Mode({ enabled: true, requireBase: false }); 
        }])
        
        //
        // When an error is thrown, show error modal.
        //
        app.factory('errorInterceptor', function ($q) {
        var preventFurtherRequests = false;

        return {
            request: function (config) {
                if (preventFurtherRequests) {
                    return;
                }
                return config || $q.when(config);
            },
            requestError: function(request){
                return $q.reject(request);
            },
            response: function (response) {
                return response || $q.when(response);
            },
            responseError: function (response) {
                //
                // Include all possible error codes and append their response to a modal.
                //
                if (response.config.url.includes("ads") == true) {
                    $('.slideIn').append('<span class="adblock">Ad Block detected</span>');
                    $('.adblock').hide().fadeIn(500);
                    $('.adblock').delay(1000).fadeOut();
                }
                if (response && response.status === 204, 302, 304, 400, 401, 403, 404, 409, 500 && response.config.url.includes("ads") == false) 
                {
                  preventFurtherRequests = false;
                  $('.errorMessage.er500, .errorOverlay').fadeIn();
                  var errorData = response.data;
                  $('.errorMessage h3').empty().append(response.status + " " + response.statusText); 
                  //
                  // When the response code is 302 and the response data is empty, the OAuth token has expired and wants to redirect. This isn't possible due to COR, so we need to force a refresh of the page.
                  //
                  if (errorData === null || errorData === '') 
                  {
                      location.reload();
                  }
                  else if (errorData.startsWith("<html xml") == true) 
                  {
                      var errorDataStrippedPre = errorData.substring(errorData.lastIndexOf("<pre>")+6,errorData.lastIndexOf("</pre>"));
                      $('.errorMessage .pre').empty().append(errorDataStrippedPre); 
                  }
                  else if (errorData.startsWith("<html>") == true) 
                  {
                      $('.errorMessage').addClass('dapUid');
                      var errorDataStripped = errorData.substring(errorData.lastIndexOf("<body>")+6,errorData.lastIndexOf("</body>"));
                      $('.errorMessage span.pre').css('white-space', 'initial');
                      $('.errorMessage .pre').empty().append(errorDataStripped); 
                      var itgenError = $('.dap-title').html();
                      $('.errorMessage .pre').css('white-space', 'none');
                      $('.errorMessage h3').empty(); 
                      $('.errorMessage h3').append(itgenError); 
                      $('.errorMessage .dap-title').hide(); 
                  }
                  else 
                  {
                    $('.errorMessage .pre').empty().append(errorData); 
                  }
                }
                if (response && response.status === 503 && response.config.url.includes("ads") == false) 
                {
                    preventFurtherRequests = false;
                    $('.errorMessage span.pre').css('white-space', 'initial');
                    $('.errorMessage.er500, .errorOverlay').fadeIn();
                    var serviceUnavailable = 'Error 503: Service unavailable';
                    $('.errorMessage h3').empty().append(serviceUnavailable); 
                    var errorData = 'Exact Online is overloaded right now. Please try again later.';
                    $('.errorMessage .pre').empty().append(errorData); 
                }
                return $q.reject(response);
            }
        };
        });
        
        app.config(function ($httpProvider) {
            $httpProvider.interceptors.push('errorInterceptor');
        });

};