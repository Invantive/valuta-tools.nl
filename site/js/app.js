//
// Set app and controller.
//
var app = angular.module('valutaBox', ['cgBusy', 'pascalprecht.translate', 'ngSanitize', 'ui.select']);

app.controller('valuta', function ($scope, $http, $translate, $location, $window, $timeout)
{
    var vm = this;

    var countryCode = $location.search().country;

    var countryNl = {"Code": "nl", "Label": "Nederland"};
    var countryBe = {"Code": "be", "Label": "België / Belgique"};
    var countryFr = {"Code": "fr", "Label": "France"};
    var countryGb = {"Code": "gb", "Label": "Great Britain"};
    var countryDe = {"Code": "de", "Label": "Deutschland"};
    var countryEs = {"Code": "es", "Label": "España"};
    var countryUs = {"Code": "us", "Label": "United States"};
    
    var hasParams = false;

    vm.myCountries = [countryNl, countryBe, countryFr, countryGb, countryDe, countryEs, countryUs];

    vm.selectedCountry = countryNl.Code;

    vm.selectedDivisions = [];
    
    vm.selectedCurrencies = [];

    vm.importMode = "custom";
    
    var date    = new Date();
    var year    = date.getFullYear();
    var month   = (date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1);
    var day     = date.getDate()  < 10 ? '0' + date.getDate()  : date.getDate();
    
    var defaultDate =  day + '-' + month + '-' + year;
    
    vm.updateSelectedCountry = function()
    {
        //
        // Get user information for picture and name in header.
        //
        var url = "auth?preset=" + countryCode + "-currency-me-query" + "&returnUrl=" + encodeURIComponent(window.location.href);
        vm.spinnerGet = $http.get(url)
               .then
                  ( function successCallback(response)
                        {
                          var isAuthenticated = response.data.isAuthenticated;
                          var authenticationUrl = response.data.authenticationUrl;

                          if (isAuthenticated)
                          {
                            //
                            // Get user information for picture and name in header.
                            //
                            vm.spinnerGet = $http.get("Preset?preset=" + countryCode + "-currency-me-query&country=" + countryCode)
                                   .then
                                      ( function successCallback(response)
                                            {
                                              $scope.mySettings = response.data.Results.mestats.Data[0];
                                              $('.userSettingImage').attr('src', 'images/default-user-icon.png');
                                              var me = response.data.Results.me.Data[0];
                                              $scope.myLanguage = me;
                                              var imageFormat = me.THUMBNAILPICTUREFORMAT;
                                              $('#username').html(me.FULLNAME);
                                              $('#usercompany').html(me.DIVISIONCUSTOMERNAME);
                                              $('#userimage').attr('src', 'images/default-user-icon.png');
                                              
                                              //
                                              // Set email for Inspectlet
                                              //
                                              __insp.push(['identify', me.EMAIL]);
                                              
                                              // Check if there is a last day loaded.
                                              if (me.OER_LAST_DAY_LOADED != null)
                                                {
                                                  $('.oer').addClass('show');
                                                  // Set title attribute of indicator to actual date value.
                                                  $('.oer.show').attr('title', me.oer_last_day_loaded_c);
                                                  // When working days age is greater than 1, show yellow indicator.
                                                  if (me.OER_WORKING_DAYS_AGE >= 1.5)
                                                    {
                                                      $('.oerdate').addClass('yellow')
                                                    }
                                                  // When working days age is equal to or greater than 2, orange indicator.
                                                  if (me.OER_WORKING_DAYS_AGE >= 3)
                                                    {
                                                      $('.oerdate').addClass('orange')
                                                    }
                                                  // When working days age is equal to or greater than 3, show red indicator.
                                                  if (me.OER_WORKING_DAYS_AGE >= 4.5)
                                                    {
                                                      $('.oerdate').addClass('red')
                                                    }
                                                }
                                              if (me.ECB_LAST_DAY_LOADED != null)
                                                {
                                                  $('.ecb').addClass('show');
                                                  $('.ecb.show').attr('title', me.ecb_last_day_loaded_c);
                                                  if (me.ECB_WORKING_DAYS_AGE >= 1.5)
                                                    {
                                                      $('.ecbdate').addClass('yellow')
                                                    }
                                                  if (me.ECB_WORKING_DAYS_AGE >= 3)
                                                    {
                                                      $('.ecbdate').addClass('orange')
                                                    }
                                                  if (me.ECB_WORKING_DAYS_AGE >= 4.5)
                                                    {
                                                      $('.ecbdate').addClass('red')
                                                    }
                                                }
                                                
                                              $translate.use(me.LANGUAGECODE.substring(0, 2));
                                              
                                              //
                                              // Append Exact API status response and add class with colour depending on status.
                                              //
                                              var sp = new StatusPage.page({ page : 'mr76vvnwb7ph' });
                                              sp.summary({
                                                success : function(data) {
                                                    var status = data.status.indicator;
                                                    var title = data.status.description;
                                                    $( ".status" ).html(status);
                                                    $(".eolStatus").prop("title", title);
                                                    $( ".status:contains('minor')" ).next().addClass('yellow');
                                                    $( ".status:contains('major')" ).next().addClass('orange');
                                                    $( ".status:contains('critical')" ).next().addClass('red');
                                                }
                                              });
                                              
                                              //
                                              // Submit form on ad button click.
                                              //
                                              $( ".buttonBridgeOnline" ).click(function() {
                                                $("input[name=email]").val(me.EMAIL);
                                                $( "._form_37" ).submit(); 
                                              });
                                              
                                              
                                              //
                                              // Get data from ad server to create commercial button. Include user data in URL.
                                              //
                                              var urlCurrent = window.location.href;
                                              //$http.get("https://ads.invantive.com/Preset?preset=retrieve&size_x=32&size_y=32&uo=" + me.DIVISIONCUSTOMERNAME + "&ue=" + me.EMAIL + "&ui=" + me.USERID + "&ud=" + me.DATACONTAINERID + "&lc=" + me.LANGUAGECODE + "&pr=" + me.APPLICATION + "&st=" + urlCurrent)
                                              // .then
                                              //    ( function successCallback(response)
                                              //        {
                                              //           var ad = response.data.Results[0].Data[0];
                                              //           //
                                              //           // Append data to commercial button.
                                              //           //
                                              //           $('.adButton').append('<a href="' + ad.tu + '" class="ad" target=_blank"><img src="' + ad.pu + '" /><span>' + ad.at + '</span></a>'); 
                                              //           if(ad.pu == undefined){
                                              //             $('.ad img').css('display', 'none');
                                              //           }
                                              //        }
                                              //    ); 
                                                  
                                                  $timeout(function() {
                                                    if (window.location.href.indexOf("divisions") > -1 || window.location.href.indexOf("number_of_days") > -1 || window.location.href.indexOf("currencies") > -1)
                                                    {
                                                      hasParams = true;
                                                      angular.element('.requestButton[ng-click="vlta.startValuta()"]').click();
                                                    }
                                                  });
                                            }
                                      );    
                          }
                          else
                          {
                              window.location.href = authenticationUrl;
                          }
                        }
                  );
    }

    if (countryCode == undefined)
    {
        $('.slideIn').fadeOut(0);
        $('.slideIn3').fadeIn();
        if ($('.slideIn3').is(":visible"))
        {
          $('.statusWrapper, .userInfoWrapper' ).hide();
        }
        else
        {
          $('.statusWrapper, .userInfoWrapper' ).show();
        }
    }
    else
    {
        vm.updateSelectedCountry();
    }

    vm.startValuta = function()
    {
        //
        // Get parameter values from URL if available
        //
        if (hasParams == true) 
        {
          $.urlParam = function(name)
          {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null)
            {
               return null;
            }
            else 
            {
               return decodeURI(results[1]) || 0;
            }
          }
          var countryFromBookmark = $.urlParam('country');
          var divisionsFromBookmark = $.urlParam('divisions');
          var currenciesFromBookmark = $.urlParam('currencies');
          var number_of_daysFromBookmark = parseInt($.urlParam('number_of_days'));
          var testrunFromBookmark = $.urlParam('testrun');
          var before_dateFromBookmark = $.urlParam('reference_date');
        }

        if ($('.bsrCurrency').hasClass('disabled')) 
        {
          return;
        }
        
        //
        // Set division URL for Angular scope.
        //
        var urlDivision = "Preset?preset=" + countryCode + "-currency-division-query";
        vm.spinnerGet = $http.get(urlDivision).then(function (response)
        {
            //
            // Get systemdivisions with DAP query from set URL.
            //
            vm.myDivisions = response.data.Results.partitions.Data;
            vm.defaultDivision = vm.myDivisions[0];
            var ld = $scope.mySettings["last_divisions"];
            //
            // If there's a parameter from the URL, use this.
            //
            if (divisionsFromBookmark && ld)
            {
              vm.lastDivisions = divisionsFromBookmark.split(",");
            }
            //
            // If there's a parameter from settings and none from the URL, use this.
            //
            else if (ld)
            {
              vm.lastDivisions = ld.split(",");
            }
            //
            // Else us default value.
            //
            else 
            {
              vm.lastDivisions = [vm.defaultDivision.CODE];
            }
            var allDivisionsDictionary = vm.myDivisions.reduce
                                          ( function(map, obj)
                                              {
                                                  map[obj.CODE] = obj;
                                          
                                                  return map;
                                              }
                                          , {}
                                          );
            var selectedDivisionsWithLabel = vm.lastDivisions.reduce
                                              ( function(arr2, divisionKey)
                                                  {
                                                      arr2.push(allDivisionsDictionary[divisionKey]);
            
                                                      return arr2;
                                                  }
                                              , []
                                              );
            
            //
            // Set the first result as default selected option for Division.
            //
              vm.selectedDivisions = selectedDivisionsWithLabel;

        });
        
        var urlCurrency = "Preset?preset=" + countryCode + "-currency-currencies-query";
        vm.spinnerGet = $http.get(urlCurrency)
        .then
        ( function (response)
          {
            //
            // Get currency list and last currencies with DAP query from set URL.
            //
            vm.myCurrencies = response.data.Results.currencies.Data;
            vm.defaultCurrencies = [{label: 'USD - US Dollar', code: 'USD'}, {label: 'EUR - Euro', code: 'EUR'}, {label: 'GBP - Pound Sterling', code: 'GBP'}];
            var lc = $scope.mySettings["last_currencies"];
            //
            // If there's a parameter from the URL, use this.
            //
            if (lc && currenciesFromBookmark)
            {
              vm.lastCurrencies = currenciesFromBookmark.split(",");
            }
            //
            // If there's a parameter from settings and none from the URL, use this.
            //
            else if (lc)
            {
              vm.lastCurrencies = lc.split(",");
            }
            //
            // Else us default values.
            //
            else
            {
              vm.lastCurrencies = ['USD', 'EUR', 'GBP'];
            }
            var allCurrenciesDictionary = vm.myCurrencies.reduce
                                          ( function(map, obj)
                                              {
                                                  map[obj.code] = obj;
                                          
                                                  return map;
                                              }
                                          , {}
                                          );

            var selectedCurrenciesWithLabel = vm.lastCurrencies.reduce
                                              ( function(arr2, currencyKey)
                                                  {
                                                      arr2.push(allCurrenciesDictionary[currencyKey]);
            
                                                      return arr2;
                                                  }
                                              , []
                                              );
            //
            // Set last selected currencies as current currencies
            //

              vm.selectedCurrencies = selectedCurrenciesWithLabel;

          }
        );
        //
        // Set default value.
        //
        vm.importDays = 90;
        vm.lastNumberOfDays = $scope.mySettings["last_number_of_days"]
        //
        // If there's a parameter from the URL, use this.
        //
        if (number_of_daysFromBookmark && vm.lastNumberOfDays)
        {
          vm.importDays = number_of_daysFromBookmark;
        }

        vm.importDate = defaultDate;
        
        if (before_dateFromBookmark && before_dateFromBookmark != defaultDate)
        {
          before_dateFromBookmark = before_dateFromBookmark.split("-").reverse().join("-");
          vm.importDate = before_dateFromBookmark;
        }
        
        //
        // If there's a parameter from settings and none from the URL, use this.
        //
        else if (vm.lastNumberOfDays)
        {
          vm.importDays = vm.lastNumberOfDays;
        }
        
        if(testrunFromBookmark == "true")
        {
          $("#testRun").prop('checked', true);
        }
        
    }

    vm.selectAll = function()
    {
      vm.selectedDivisions = vm.myDivisions;
    }
    
    vm.selectDefault = function()
    {
      vm.selectedDivisions = [vm.defaultDivision];
    }
    
    vm.selectAllCurrencies = function()
    {
      vm.selectedCurrencies = vm.myCurrencies;
    }
    
    vm.selectDefaultCurrency = function()
    {
      vm.selectedCurrencies = vm.defaultCurrencies;
    }
    
    vm.copyToClipboard = function()
    {
      var divisionsCopy = "";
      var firstCopy = true;
      for (var copyDivision in vm.selectedDivisions)
      {
       if (firstCopy)
       {
         firstCopy = false;
       }
       else
       {
         divisionsCopy = divisionsCopy + ",";
       }
       divisionsCopy = divisionsCopy + vm.selectedDivisions[copyDivision].CODE;
      }
      var currenciesCopy = "";
      var firstCopy = true;
      for (var copyCurrency in vm.selectedCurrencies)
      {
       if (firstCopy)
       {
         firstCopy = false;
       }
       else
       {
         currenciesCopy = currenciesCopy + ",";
       }
       currenciesCopy = currenciesCopy + vm.selectedCurrencies[copyCurrency].code;
      }
      
      var daysCopy;
      daysCopy = vm.importDays;
      
      var testrunCopy = "false";
      if ($("#testRun").prop("checked") == true) 
      {
        testrunCopy = "true"; 
      }
      
      var beforeDateCopy;
      
      if (vm.importDate != defaultDate) 
      {
        beforeDateCopy = vm.importDate;
        beforeDateCopy = beforeDateCopy.split("-").reverse().join("-");
        var urlParamsCopy = "?country=" + countryCode + "&divisions=" + divisionsCopy + "&number_of_days=" + daysCopy + "&currencies=" + currenciesCopy + "&testrun=" + testrunCopy + "&reference_date=" + beforeDateCopy;
      }
      else 
      {
        var urlParamsCopy = "?country=" + countryCode + "&divisions=" + divisionsCopy + "&number_of_days=" + daysCopy + "&currencies=" + currenciesCopy + "&testrun=" + testrunCopy;
      }

      var copiedURL = 'https://' + $location.host() + $location.path() + urlParamsCopy;
      $location.url($location.path() + urlParamsCopy);
      
      var createCopiedUrl = document.createElement('input'), text = copiedURL;
      document.body.appendChild(createCopiedUrl);
      createCopiedUrl.value = text;
      createCopiedUrl.select();
      document.execCommand('copy');
      document.body.removeChild(createCopiedUrl);
      
      $('.copiedClipboard, .overlayModal').fadeIn('fast').delay(1500).fadeOut('fast');
    }

    vm.importCurrencyCustom = function()
    {
        //
        // Set division URL for Angular scope.
        //
        var divisions = "";
        var first = true;
        for (var aDivision in vm.selectedDivisions)
        {
         if (first)
         {
           first = false;
         }
         else
         {
           divisions = divisions + ",";
         }
         divisions = divisions + vm.selectedDivisions[aDivision].CODE;
        }
        var currencies = "";
        var first = true;
        for (var aCurrency in vm.selectedCurrencies)
        {
         if (first)
         {
           first = false;
         }
         else
         {
           currencies = currencies + ",";
         }
         currencies = currencies + vm.selectedCurrencies[aCurrency].code;
        }

        var days;
        days = vm.importDays;
        
        var testrun = "false";
        if ($("#testRun").prop("checked") == true) 
        {
          testrun = "true";
        }
                
        if (vm.importDate != defaultDate) 
        {
          beforeDate = vm.importDate;
          beforeDate = beforeDate.split("-").reverse().join("-");
          var urlCurrencyCustom = "Preset?preset=" + countryCode + "-currency-import-query&country=" + countryCode + "&divisions=" + divisions + "&number_of_days=" + days + "&currencies=" + currencies + "&testrun=" + testrun + "&reference_date=" + beforeDate;
        }
        else 
        {
          var urlCurrencyCustom = "Preset?preset=" + countryCode + "-currency-import-query&country=" + countryCode + "&divisions=" + divisions + "&number_of_days=" + days + "&currencies=" + currencies + "&testrun=" + testrun;
        }

        vm.spinnerGet = $http.get(urlCurrencyCustom)
                             .then
                             ( function successCallback(response)
                                 {
                                   var currenciesDisplay = response.data.Results.importstats.Data[0];
                                   var importCompanies = response.data.Results.importcompanies.Data;
                                   var importOldRates = response.data.Results.importoldrates.Data;
                                   var importDetails = response.data.Results.importdetails.Data;
                                   var today = new Date();
                                   var date = today.getFullYear()+'-'+(today.getMonth()-2)+'-'+today.getDate();
                                   var month = today.getMonth()+1;
                                   var day = today.getDate();
                                   var dateToday = today.getFullYear()+(month<10 ? '0' : '')+ month +(day<10 ? '0' : '') + day;
                                   var firstImport = new Date($scope.mySettings.first_import_date.split('T')[0]);
                                   var firstImportDate = firstImport.getFullYear()+'-'+(firstImport.getMonth()+1)+'-'+firstImport.getDate();
                                   if ($scope.myLanguage.LANGUAGECODE.substring(0, 2) != 'nl')
                                   {
                                       //
                                       // English
                                       //
                                       var reviewURL = "https://apps.exactonline.com/nl/en-US/V2/Apps?applicationName=valuta-tools&partnerName=invantive-b-v&search=invantive&sortType=Relevance&tab=Reviews";
                                       $('.overlayModal').after
                                       (
                                         "<div class='successModal'><span class='close'></span><div><span class='importSuccessfull'>Import successfull.</span></div><div><span class='currenciesLoaded'></span></div><div><span class='selectionLoaded'></span><span class='baseCurrency'></span></div><div class='downloadReport'>Download report</div></div>"
                                       );
                                       if (testrun == 'true')
                                       {
                                           $('.importSuccessfull').html("Import run in test mode. In normal mode, the results would have been:");
                                       }
                                       if (currenciesDisplay.CNT_RATES_LOADED > 1)
                                       {
                                           $('.currenciesLoaded').html(currenciesDisplay.CNT_RATES_LOADED + ' exchange rates loaded for ' + currenciesDisplay.CNT_DIVISIONS_LOADED + ' companies.');
                                       }
                                       else if (currenciesDisplay.CNT_RATES_LOADED == 1)
                                       {
                                           $('.currenciesLoaded').html(currenciesDisplay.CNT_RATES_LOADED + ' exchange rate loaded for ' + currenciesDisplay.CNT_DIVISIONS_LOADED + ' companies.');
                                       }
                                       else if (currenciesDisplay.CNT_RATES_LOADED == 0)
                                       {
                                           $('.currenciesLoaded').html('No exchange rates loaded.');
                                       }
                                       if (currenciesDisplay.CNT_DIVISIONS_SELECTED > 1)
                                       {
                                           $('.selectionLoaded').html('Selection included ' + currenciesDisplay.CNT_DIVISIONS_SELECTED + ' companies');
                                       }
                                       else if (currenciesDisplay.CNT_DIVISIONS_SELECTED == 1)
                                       {
                                           $('.selectionLoaded').html('Selection included one company');
                                       }
                                       else if (currenciesDisplay.CNT_DIVISIONS_SELECTED == 0)
                                       {
                                           $('.selectionLoaded').html('Selection included no company');
                                       }
                                       
                                       if (currenciesDisplay.CNT_DISTINCT_BASE_CURRENCIES > 1)
                                       {
                                           $('.baseCurrency').html(' with ' + currenciesDisplay.CNT_RATES_LOADED + ' base currencies: ' + currenciesDisplay.DISTINCT_BASE_CURRENCIES + '.');
                                       }
                                       else if (currenciesDisplay.CNT_DISTINCT_BASE_CURRENCIES == 1)
                                       {
                                           $('.baseCurrency').html(' with a single base currency ' + currenciesDisplay.DISTINCT_BASE_CURRENCIES + '.');
                                       }
                                       else if (currenciesDisplay.CNT_DISTINCT_BASE_CURRENCIES == 0)
                                       {
                                           $('.baseCurrency').html(' with no base currency.');
                                       }
                                       
                                       if ($scope.mySettings.cnt_import > 10 && firstImportDate < date && countryCode == "nl")
                                       {
                                       $('.downloadReport').after
                                          (
                                            "<a class='leaveReview' target='_blank' href='" + reviewURL + "'>Help others with a public review.</a>"
                                          );
                                       } 
                                   }
                                   else 
                                   {
                                       var reviewURL = "https://apps.exactonline.com/nl/nl-NL/V2/Apps?applicationName=valuta-tools&partnerName=invantive-b-v&search=invantive&sortType=Relevance&tab=Reviews";
                                       $('.overlayModal').after
                                       (
                                         "<div class='successModal'><span class='close'></span><div><span class='importSuccessfull'>Inladen geslaagd.</span></div><div><span class='currenciesLoaded'></span></div><div><span class='selectionLoaded'></span><span class='baseCurrency'></span></div><div class='downloadReport'>Download rapport</div></div>"
                                       );
                                       if (testrun == 'true')
                                       {
                                           $('.importSuccessfull').html("Import uitgevoerd in testmodus. In normale modus zouden dit de resultaten geweest zijn:");
                                       }
                                       if (currenciesDisplay.CNT_RATES_LOADED > 1)
                                       {
                                           $('.currenciesLoaded').html(currenciesDisplay.CNT_RATES_LOADED + ' wisselkoersen geladen voor ' + currenciesDisplay.CNT_DIVISIONS_LOADED + ' administraties.');
                                       }
                                       else if (currenciesDisplay.CNT_RATES_LOADED == 1)
                                       {
                                           $('.currenciesLoaded').html(currenciesDisplay.CNT_RATES_LOADED + ' wisselkoers geladen voor ' + currenciesDisplay.CNT_DIVISIONS_LOADED + ' administraties.');
                                       }
                                       else if (currenciesDisplay.CNT_RATES_LOADED == 0)
                                       {
                                           $('.currenciesLoaded').html('Geen wisselkoersen geladen.');
                                       }
                                       if (currenciesDisplay.CNT_DIVISIONS_SELECTED > 1)
                                       {
                                           $('.selectionLoaded').html('Selectie bevatte ' + currenciesDisplay.CNT_DIVISIONS_SELECTED + ' administraties');
                                       }
                                       else if (currenciesDisplay.CNT_DIVISIONS_SELECTED == 1)
                                       {
                                           $('.selectionLoaded').html('Selectie bevatte een administratie');
                                       }
                                       else if (currenciesDisplay.CNT_DIVISIONS_SELECTED == 0)
                                       {
                                           $('.selectionLoaded').html('Selectie bevatte geen administratie');
                                       }
                                       
                                       if (currenciesDisplay.CNT_DISTINCT_BASE_CURRENCIES > 1)
                                       {
                                           $('.baseCurrency').html(' met ' + currenciesDisplay.CNT_RATES_LOADED + ' basisvaluta: ' + currenciesDisplay.DISTINCT_BASE_CURRENCIES + '.');
                                       }
                                       else if (currenciesDisplay.CNT_DISTINCT_BASE_CURRENCIES == 1)
                                       {
                                           $('.baseCurrency').html(' met een enkele basisvaluta ' + currenciesDisplay.DISTINCT_BASE_CURRENCIES + '.');
                                       }
                                       else if (currenciesDisplay.CNT_DISTINCT_BASE_CURRENCIES == 0)
                                       {
                                           $('.baseCurrency').html(' zonder basisvaluta.');
                                       }
                                       
                                       if ($scope.mySettings.cnt_import > 10 && firstImportDate < date && countryCode == "nl")
                                       {
                                       $('.downloadReport').after
                                          (
                                            "<a class='leaveReview' target='_blank' href='" + reviewURL + "'>Help anderen met een openbare review.</a>"
                                          );
                                       } 
                                   }

                                   $('body').off().on('click', '.downloadReport', function () 
                                   {
                                       $.getScript("js/XSLExport.js");
                                       var createXLSLSummaryObj = [];
                                       var createXLSLParamObj = [];
                                       var createXLSLCompanyObj = [];
                                       var createXLSLOldRatesObj = [];
                                       var createXLSLDetailsObj = [];
                                       var currencyArray = [];
                                       var companyArray = [];
                                       var currencyArray = [];
                                       var currencyArray = [];
                                        
                                       if ($scope.myLanguage.LANGUAGECODE.substring(0, 2) == 'nl')
                                       {
                                         var filename = 'Valuta Import Actie Rapport-' + dateToday + '.xlsx';
                                         var headerParamArray = ['Land', 'Dagen', 'Testrun', 'Gebruiker', 'Bedrijf', 'Referentie Datum', 'Administraties', 'Valuta'];
                                         var headerSummaryArray = ['Aantal Wisselkoersen Geladen',	'Aantal Bedrijven Geladen',	'Aantal Bedrijven Geselecteerd',	'Aantal Verschillende Basisvaluta',	'Verschillende Basisvaluta'];
                                         var headerCompanyArray = ['Abonnementshouder code', 'Abonnementshouder Naam', 'Korte Naam', 'Naam', 'Administratievaluta', 'Administratie ID', 'Naam Administratie', 'Adres', 'Plaats', 'Staat/Provincie', 'Postcode', 'Land', 'E-mailadres', 'Telefoon', 'Website', 'KvK nummer', 'BTW Nummer'];
                                         var headerDetailsArray = ['Korte Naam Administratie', 'Naam Administratie', 'Startdatum', 'Bronvaluta', 'Doelvaluta', 'Wisselkoers', 'Sleutel'];
                                         var headerOldRatesArray = ['Korte Naam Administratie', 'Naam Administratie', 'Dag', 'Bronvaluta', 'Doelvaluta', 'Wisselkoers', 'Bewerkt', 'Bewerker', 'Gemaakt', 'Aanmaker', 'Sleutel'];
                                         var parametersSheetName = 'Parameters';
                                         var summarySheetName = 'Samenvatting';
                                         var companySheetName = 'Administraties';
                                         var oldRatesSheetName = 'Bestaande Koersen';
                                         var detailsSheetName = 'Details';
                                       }                              
                                       else
                                       {
                                         var filename = 'Currency Import Action Report-' + dateToday + '.xlsx';
                                         var headerParamArray = ['Country', 'Days', 'Test Run', 'User', 'Company', 'Reference Date', 'Companies', 'Currencies'];
                                         var headerSummaryArray = ['Number Rates Loaded',	'Number Companies Loaded',	'Number Companies Selected',	'Number Distinct Base Currencies',	'Distinct Base Currencies'];
                                         var headerCompanyArray = ['Subscription Owner Code', 'Subscription Owner name', 'Short Name', 'Name', 'Company Currency', 'Company ID', 'Company Name', 'Address', 'City', 'State/Province', 'ZIP Code', 'Country', 'Email Address', 'Phone', 'Website', 'CoC Number', 'VAT Number'];
                                         var headerDetailsArray = ['Company Short Name', 'Company Name', 'Date', 'Source Currency', 'Target Currency', 'Exchange Rate', 'Key'];
                                         var headerOldRatesArray = ['Company Short Name', 'Company Name', 'Day', 'Source Currency', 'Target Currency', 'Exchange Rate', 'Modified', 'Modifier', 'Created', 'Creator', 'Key'];
                                         var parametersSheetName = 'Parameters';
                                         var summarySheetName = 'Summary';
                                         var companySheetName = 'Companies';
                                         var oldRatesSheetName = 'Existing Rates';
                                         var detailsSheetName = 'Details';
                                       }
                                       var user = $scope.myLanguage.FULLNAME;
                                       var divisionuser = $scope.myLanguage.DIVISIONCUSTOMERNAME;
                                       var paramArray = [countryCode, days, testrun, user, divisionuser, beforeDate, divisions, currencies];
                                       
                                       /* XLS Head Columns */
                                       $.each(currenciesDisplay, function (key, value) {
                                           currencyArray.push(value);
                                       });
                                       
                                       createXLSLCompanyObj.push(headerCompanyArray);
                                       $.each(importCompanies, function (index, value) {
                                        var importCompaniesRows = [];
                                         $.each(value, function (ind, val) {
                                             importCompaniesRows.push(val);
                                         }); 
                                         createXLSLCompanyObj.push(importCompaniesRows);
                                       }); 
                                       
                                       
                                       createXLSLOldRatesObj.push(headerOldRatesArray); 
                                       $.each(importOldRates, function (index, value) {
                                        var importOldRatesRows = [];
                                         $.each(value, function (ind, val) {
                                             importOldRatesRows.push(val);
                                         }); 
                                         createXLSLOldRatesObj.push(importOldRatesRows);
                                       }); 
                                       
                                       createXLSLDetailsObj.push(headerDetailsArray);
                                       $.each(importDetails, function (index, value) {
                                        var importDetailsData = [];
                                         $.each(value, function (ind, val) {
                                             importDetailsData.push(val);
                                         }); 
                                         createXLSLDetailsObj.push(importDetailsData);
                                       }); 
                                       
                                       createXLSLSummaryObj.push(headerSummaryArray, currencyArray);
                                       
                                       createXLSLParamObj.push(headerParamArray, paramArray);
                                       
                                       /* File Name */
                                       /* Sheet Name */
                                       var divisionsLength = 12;
                                       if (divisions.length > 12)
                                       {
                                         var divisionsLength = divisions.length
                                       }
                                       var currenciesLength = 12;
                                       if (currencies.length > 12)
                                       {
                                         var currenciesLength = currencies.length + 1
                                       } 
                                       
                                       var wb = XLSX.utils.book_new(),
                                       companies = XLSX.utils.aoa_to_sheet(createXLSLCompanyObj);
                                       companies['!cols'] = [
                                         { wch:10 },
                                         { wch:30 },
                                         { wch:12 },
                                         { wch:30 },
                                         { wch:20 },
                                         { wch:15 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:20 },
                                         { wch:13 },
                                         { wch:12 },
                                         { wch:8 },
                                         { wch:30 },
                                         { wch:15 },
                                         { wch:35 },
                                         { wch:15 },
                                         { wch:15 }
                                       ]
                                       oldrates = XLSX.utils.aoa_to_sheet(createXLSLOldRatesObj);
                                       oldrates['!cols'] = [
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:50 }
                                       ]
                                       details = XLSX.utils.aoa_to_sheet(createXLSLDetailsObj);
                                       details['!cols'] = [
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:50 }
                                       ]
                                       summary = XLSX.utils.aoa_to_sheet(createXLSLSummaryObj);
                                       summary['!cols'] = [
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 },
                                         { wch:30 }
                                       ]
                                       params = XLSX.utils.aoa_to_sheet(createXLSLParamObj);
                                       params['!cols'] = [
                                         { wch:6 },
                                         { wch:5 },
                                         { wch:8 },
                                         { wch:user.length },
                                         { wch:8 },
                                         { wch:15 },
                                         { wch:divisionsLength },
                                         { wch:currenciesLength }
                                       ]
                                       /* Add worksheet to workbook */
                                       XLSX.utils.book_append_sheet(wb, params, parametersSheetName);
                                       XLSX.utils.book_append_sheet(wb, summary, summarySheetName);
                                       XLSX.utils.book_append_sheet(wb, companies, companySheetName);
                                       XLSX.utils.book_append_sheet(wb, oldrates, oldRatesSheetName);
                                       XLSX.utils.book_append_sheet(wb, details, detailsSheetName);
                                       /* Write workbook and Download */
                                       XLSX.writeFile(wb, filename);
                                   });
                                   
                                   
                                   $('.successModal, .errorOverlay').fadeIn('fast');
                                   $( '.successModal .close, .errorOverlay' ).click(function() 
                                     {
                                       $('.successModal, .errorOverlay').fadeOut('fast');
                                       $( ".successModal" ).remove();
                                     }
                                   );
                                   $(document).keyup(function(e)
                                   {
                                     if(e.keyCode === 27)
                                     {
                                       $('.successModal, .errorOverlay').fadeOut('fast');
                                       $( ".successModal" ).remove();
                                     }
                                   });
                                   //
                                   // Set parameters for bookmark
                                   //
                                   if (vm.importDate != defaultDate) 
                                   {
                                     beforeDate = vm.importDate;
                                     beforeDate = beforeDate.split("-").reverse().join("-");
                                     var urlParams = "?country=" + countryCode + "&divisions=" + divisions + "&number_of_days=" + days + "&currencies=" + currencies + "&testrun=" + testrun + "&reference_date=" + beforeDate;
                                   }
                                   else 
                                   {
                                     var urlParams = "?country=" + countryCode + "&divisions=" + divisions + "&number_of_days=" + days + "&currencies=" + currencies + "&testrun=" + testrun;
                                   }
                                   //
                                   // Replace current URL with new parameters
                                   //
                                   var currentPath = $location.path();
                                   $location.url(currentPath + urlParams);
                                 }
                             );
        return vm.config;
   }

});

app.filter('propsFilter', function()
{
  return function(items, props)
  {
    var out = [];

    if (angular.isArray(items))
    {
      var keys = Object.keys(props);

      items.forEach(function(item)
      {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    }
    else
    {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});

//
// Create translation library.
//
app.config(['$translateProvider', function ($translateProvider)
{
    $translateProvider.preferredLanguage('en');
    $translateProvider.addInterpolation('$translateMessageFormatInterpolation');
    $translateProvider.useSanitizeValueStrategy('sanitize');

    $translateProvider.translations('en', {
        itgen_ct_heading: 'Currency Tools',
        itgen_ct_toolname: 'Daily Exchange Rates',
        itgen_ct_import: 'Import latest exchange rates',
        itgen_ct_import_rates: 'Import Exchange Rates',
        itgen_ct_division: 'Exact Online Companies',
        itgen_ct_success: 'Import successful.',
        itgen_ct_country: 'Country',
        itgen_ct_choose_country: 'Choose country',
        itgen_ct_powered: 'Powered by Invantive SQL',
        itgen_ct_oer: 'Open Exchange Rates',
        itgen_ct_ecb: 'European Central Bank',
        itgen_ct_currency: 'Currencies',
        itgen_ct_importsingle: 'Import',
        itgen_ct_review: 'Leave a review on Google',
        itgen_ct_question: 'Ask a question',
        itgen_ct_idea: 'Suggest an idea',
        itgen_ct_problem: 'Report a problem',
        itgen_ct_days: 'days prior to',
        itgen_ct_selectall: 'Select All',
        itgen_ct_selectdefault: 'Select Default',
        itgen_ct_goback: 'Go to main menu.',
        itgen_ct_selectalltitle: 'Add all Exact Online companies to the selection.',
        itgen_ct_selectdefaulttitle: 'Reset the selection to company last used in Exact Online.',
        itgen_ct_helpbutton: 'Help',
        itgen_ct_helpbuttontt: 'Click here for additional help.',
        itgen_ct_first_import_date: 'First import date',
        itgen_ct_last_import_date: 'Last import date',
        itgen_ct_total_imports: 'Total imports',
        itgen_ct_total_rates_imported: 'Total rates imported',
        itgen_ct_total_duration: 'Total duration',
        itgen_ct_statistics: 'Statistics',
        itgen_ct_seconds: 'seconds',
        itgen_ct_copied: 'Successfully copied to clipboard.',
        itgen_ct_copy: 'Copy to clipboard.',
        itgen_ct_form_division: 'At least one company must be selected. Please add one or more companies to your selection.',
        itgen_ct_form_currency: 'At least one currency must be selected. Please add one or more currencies to your selection.',
        itgen_ct_testrun: 'Test run',
        itgen_ct_logo_text: 'Currency Tools for Exact Online Logo',
        itgen_ct_cloud_button: 'Try Exact Online with Power BI 180 days',
        itgen_ct_data_container_id: 'Data Container ID',
        itgen_ct_user_settings: 'User Settings',
        itgen_ct_attention_cloud: 'Attention!',
        itgen_ct_contact_nl: 'the Netherlands',
        itgen_ct_contact_sales: 'Sales',
        itgen_ct_contact_support: 'Support',
        itgen_ct_contact_office_hours: 'Office hours',
        itgen_ct_support_portal: 'Support Portal',
        itgen_ct_attention_cloud_text: 'Prepare yourself timely for the migration in 2022 of Valuta Tools to Invantive Cloud, ',
        itgen_ct_attention_cloud_steps: 'Read the steps here.',
        itgen_ct_attention_jquery: 'jQuery is disabled by software or administrator. Ask your IT-administrator to allow jQuery to work with Valuta Tools.',
        itgen_ct_contact_email_label: 'Email',
        itgen_ct_contact_email_url: 'info@invantive.com',
        itgen_ct_contact_web_url: 'https://invantive.com',
        itgen_ct_contact_web_label: 'invantive.com',
        itgen_ct_contact_web: 'Web',
        itgen_ct_contact_heading: 'Contact',
        itgen_ct_contact_button: 'Contact'
    });

    $translateProvider.translations('de', {
        itgen_ct_heading: 'Währungstools',
        itgen_ct_toolname: 'Tägliche Wechselkurse',
        itgen_ct_import: 'Importieren Sie die neuesten Wechselkurse',
        itgen_ct_import_rates: 'Wechselkurse importieren',
        itgen_ct_division: 'Exakte Online-Unternehmen',
        itgen_ct_success: 'Import erfolgreich.',
        itgen_ct_country: 'Land',
        itgen_ct_choose_country: 'Wähle das Land',
        itgen_ct_powered: 'Angetrieben von Invantive SQL',
        itgen_ct_oer: 'Open Exchange Rates',
        itgen_ct_ecb: 'Europäische Zentralbank',
        itgen_ct_importsingle: 'Importieren',
        itgen_ct_review: 'Hinterlassen Sie eine Bewertung bei Google',
        itgen_ct_question: 'Frage stellen',
        itgen_ct_idea: 'Eine Idee vorschlagen',
        itgen_ct_problem: 'Problem melden',
        itgen_ct_days: 'Tage vor',
        itgen_ct_selectall: 'Alle auswählen',
        itgen_ct_selectdefault: 'Standard auswählen',
        itgen_ct_goback: 'Gehe zum Hauptmenü.',
        itgen_ct_selectalltitle: 'Fügen Sie alle Exact Online-Unternehmen zur Auswahl hinzu.',
        itgen_ct_selectdefaulttitle: 'Setzen Sie die Auswahl auf das zuletzt in Exact Online verwendete Unternehmen zurück.',
        itgen_ct_helpbutton: 'Hilfe',
        itgen_ct_helpbuttontt: 'Klicken Sie hier für weitere Hilfe.',
        itgen_ct_first_import_date: 'Datum ersten Imports',
        itgen_ct_last_import_date: 'Datum letzten Imports',
        itgen_ct_total_imports: 'Importe insgesamt',
        itgen_ct_total_rates_imported: 'Importierte Wechselkurse insgesamt',
        itgen_ct_total_duration: 'Dauer insgesamt',
        itgen_ct_statistics: 'Statistik',
        itgen_ct_seconds: 'seconds',
        itgen_ct_currency: 'Währungen',
        itgen_ct_copied: 'Erfolgreich in die Zwischenablage kopiert.',
        itgen_ct_copy: 'In Zwischenablage kopieren.',
        itgen_ct_form_division: 'Mindestens ein Unternehmen muss ausgewählt werden. Bitte fügen Sie ein oder mehrere Unternehmen zu Ihrer Auswahl hinzu.',
        itgen_ct_form_currency: 'Mindestens eine Währung muss ausgewählt werden. Bitte fügen Sie eine oder mehrere Währungen zu Ihrer Auswahl hinzu.',
        itgen_ct_testrun: 'Testlauf',
        itgen_ct_logo_text: 'Logo Währungstools für Exact Online',
        itgen_ct_cloud_button: '180 Tage Exact Online mit Power BI testen',
        itgen_ct_data_container_id: 'Datenkontainer ID',
        itgen_ct_user_settings: 'Benutzereinstellungen',
        itgen_ct_attention_cloud: 'Achtung!',
        itgen_ct_contact_nl: 'die Niederlande',
        itgen_ct_contact_sales: 'Verkauf',
        itgen_ct_contact_support: 'Unterstützung',
        itgen_ct_contact_office_hours: 'Bürozeiten',
        itgen_ct_support_portal: 'Unterstützungsportal',
        itgen_ct_attention_cloud_text: 'Bereiten Sie sich rechtzeitig auf die Migration von Valuta Tools auf Invantive Cloud im Jahr 2022 vor.',
        itgen_ct_attention_cloud_steps: 'Lesen Sie die Schritte hier.',
        itgen_ct_attention_jquery: 'jQuery ist von der Software oder dem Administrator deaktiviert. Bitten Sie Ihren IT-Administrator jQuery die Arbeit mit Valuta Tools zu ermöglichen.',
        itgen_ct_contact_email_label: 'E-mail',
        itgen_ct_contact_email_url: 'info@invantive.com',
        itgen_ct_contact_web_url: 'https://invantive.com',
        itgen_ct_contact_web_label: 'invantive.com',
        itgen_ct_contact_web: 'Web',
        itgen_ct_contact_heading: 'Contact',
        itgen_ct_contact_button: 'Contact'
    });

    $translateProvider.translations('fr', {
        itgen_ct_heading: 'Currency Tools',
        itgen_ct_toolname: 'Daily Exchange Rates',
        itgen_ct_import: 'Import latest exchange rates',
        itgen_ct_import_rates: 'Import Exchange Rates',
        itgen_ct_division: 'Exact Online Companies',
        itgen_ct_success: 'Import successful.',
        itgen_ct_country: 'Country',
        itgen_ct_choose_country: 'Choose country',
        itgen_ct_powered: 'Powered by Invantive SQL',
        itgen_ct_oer: 'Open Exchange Rates',
        itgen_ct_ecb: 'European Central Bank',
        itgen_ct_importsingle: 'Import',
        itgen_ct_review: 'Leave a review on Google',
        itgen_ct_question: 'Ask a question',
        itgen_ct_idea: 'Suggest an idea',
        itgen_ct_problem: 'Report a problem',
        itgen_ct_days: 'days prior to',
        itgen_ct_selectall: 'Select All',
        itgen_ct_selectdefault: 'Select Default',
        itgen_ct_goback: 'Go to main menu.',
        itgen_ct_selectalltitle: 'Add all Exact Online companies to the selection.',
        itgen_ct_selectdefaulttitle: 'Reset the selection to company last used in Exact Online.',
        itgen_ct_helpbutton: 'Help',
        itgen_ct_helpbuttontt: 'Click here for additional help.',
        itgen_ct_first_import_date: 'First import date',
        itgen_ct_last_import_date: 'Last import date',
        itgen_ct_total_imports: 'Total imports',
        itgen_ct_total_rates_imported: 'Total rates imported',
        itgen_ct_total_duration: 'Total duration',
        itgen_ct_statistics: 'Statistics',
        itgen_ct_seconds: 'seconds',
        itgen_ct_currency: 'Currencies',
        itgen_ct_copied: 'Successfully copied to clipboard.',
        itgen_ct_copy: 'Copy to clipboard.',
        itgen_ct_form_division: 'At least one company must be selected. Please add one or more companies to your selection.',
        itgen_ct_form_currency: 'At least one currency must be selected. Please add one or more currencies to your selection.',
        itgen_ct_testrun: 'Test run',
        itgen_ct_logo_text: 'Currency Tools for Exact Online Logo',
        itgen_ct_cloud_button: 'Try Exact Online with Power BI 180 days',
        itgen_ct_data_container_id: 'Data Container ID',
        itgen_ct_user_settings: 'User Settings',
        itgen_ct_attention_cloud: 'Attention!',
        itgen_ct_contact_nl: 'the Netherlands',
        itgen_ct_contact_sales: 'Sales',
        itgen_ct_contact_support: 'Support',
        itgen_ct_contact_office_hours: 'Office hours',
        itgen_ct_support_portal: 'Support Portal',
        itgen_ct_attention_cloud_text: 'Prepare yourself timely for the migration in 2022 of Valuta Tools to Invantive Cloud.',
        itgen_ct_attention_cloud_steps: 'Read the steps here.',
        itgen_ct_attention_jquery: 'jQuery is disabled by software or administrator. Ask your IT-administrator to allow jQuery to work with Valuta Tools.',
        itgen_ct_contact_email_label: 'Email',
        itgen_ct_contact_email_url: 'info@invantive.com',
        itgen_ct_contact_web_url: 'https://invantive.com',
        itgen_ct_contact_web_label: 'invantive.com',
        itgen_ct_contact_web: 'Web',
        itgen_ct_contact_heading: 'Contact',
        itgen_ct_contact_button: 'Contact'
    });

    $translateProvider.translations('es', {
        itgen_ct_heading: 'Herramientas de Divisas',
        itgen_ct_toolname: 'Tipos de Cambio Diarios',
        itgen_ct_import: 'Importar los últimos tipos de cambio',
        itgen_ct_import_rates: 'Importar Tipos de Cambio',
        itgen_ct_division: 'Empresas en Exact Online',
        itgen_ct_success: 'Importación exitosa.',
        itgen_ct_country: 'País',
        itgen_ct_choose_country: 'Seleccionar un país',
        itgen_ct_powered: 'Desarrollado por Invantive SQL',
        itgen_ct_oer: 'Open Exchange Rates',
        itgen_ct_ecb: 'Banco Central Europeo',
        itgen_ct_importsingle: 'Importar',
        itgen_ct_review: 'Dejar un comentario en Google',
        itgen_ct_question: 'Hacer una pregunta',
        itgen_ct_idea: 'Sugerir una idea',
        itgen_ct_problem: 'Reportar un problema',
        itgen_ct_days: 'días antes de',
        itgen_ct_selectall: 'Seleccionar Todo',
        itgen_ct_selectdefault: 'Seleccionar Predeterminado',
        itgen_ct_goback: 'Ir al menú principal.',
        itgen_ct_selectalltitle: 'Añada todas las empresas de Exact Online a la selección.',
        itgen_ct_selectdefaulttitle: 'Restablecer la selección a la empresa utilizada por última vez en Exact Online.',
        itgen_ct_helpbutton: 'Ayuda',
        itgen_ct_helpbuttontt: 'Haga clic aquí para obtener ayuda adicional.',
        itgen_ct_first_import_date: 'Fecha de primera importación',
        itgen_ct_last_import_date: 'Fecha de última importación',
        itgen_ct_total_imports: 'Importaciones totales',
        itgen_ct_total_rates_imported: 'Total de tasas importadas',
        itgen_ct_total_duration: 'Duración total',
        itgen_ct_statistics: 'Estadísticas',
        itgen_ct_seconds: 'seconds',
        itgen_ct_currency: 'Divisas',
        itgen_ct_copied: 'Copiado correctamente al portapapeles.',
        itgen_ct_copy: 'Copiar al portapapeles.',
        itgen_ct_form_division: 'Debe seleccionarse al menos una empresa. Por favor añada una o más empresas a su selección.',
        itgen_ct_form_currency: 'Se debe seleccionar al menos una moneda. Añada una o más monedas a su selección.',
        itgen_ct_testrun: 'Ejecución de pruebas',
        itgen_ct_logo_text: 'Herramientas de Divisas para el Logotipo Exact Online',
        itgen_ct_cloud_button: 'Prueba Exact Online con Power BI 180 días',
        itgen_ct_data_container_id: 'ID del Contenedor de Datos',
        itgen_ct_user_settings: 'Configuración de usuario',
        itgen_ct_attention_cloud: '¡Atención!',
        itgen_ct_contact_nl: 'Los Países Bajos',
        itgen_ct_contact_sales: 'Ventas',
        itgen_ct_contact_support: 'Soporte',
        itgen_ct_contact_office_hours: 'Horario de Oficina',
        itgen_ct_support_portal: 'Portal de Soporte',
        itgen_ct_attention_cloud_text: 'Prepárese oportunamente para la migración en 2022 de Valuta Tools a Invantive Cloud.',
        itgen_ct_attention_cloud_steps: 'Lee los pasos aquí.',
        itgen_ct_attention_jquery: 'jQuery está deshabilitado por el software o por el administrador. Pida a su administrador de TI que permita que jQuery funcione con Valuta Tools.',
        itgen_ct_contact_email_label: 'Correo Electrónico',
        itgen_ct_contact_email_url: 'info@invantive.com',
        itgen_ct_contact_web_url: 'https://invantive.com',
        itgen_ct_contact_web_label: 'invantive.com',
        itgen_ct_contact_web: 'Web',
        itgen_ct_contact_heading: 'Contact',
        itgen_ct_contact_button: 'Contact'
    });

    $translateProvider.translations('nl', {
        itgen_ct_heading: 'Valuta Tools',
        itgen_ct_toolname: 'Dagwisselkoersen',
        itgen_ct_import: 'Inlezen laatste wisselkoersen',
        itgen_ct_import_custom: 'Inlezen wisselkoersen laatste',
        itgen_ct_days: 'dagen voorafgaand aan',
        itgen_ct_import_rates: 'Importeer Wisselkoersen',
        itgen_ct_division: 'Exact Online Administraties',
        itgen_ct_success: 'Inlezen geslaagd.',
        itgen_ct_country: 'Land',
        itgen_ct_choose_country: 'Kies land',
        itgen_ct_powered: 'Aangedreven door Invantive SQL',
        itgen_ct_oer: 'Open Exchange Rates',
        itgen_ct_ecb: 'Europese Centrale Bank',
        itgen_ct_currency: 'Valuta',
        itgen_ct_importsingle: 'Inlezen',
        itgen_ct_review: 'Laat een review achter op Google',
        itgen_ct_question: 'Stel een vraag',
        itgen_ct_idea: 'Stel een idee voor',
        itgen_ct_problem: 'Rapporteer een probleem',
        itgen_ct_selectall: 'Alles selecteren',
        itgen_ct_selectdefault: 'Standaard selecteren',
        itgen_ct_goback: 'Terug naar hoofdmenu.',
        itgen_ct_selectalltitle: 'Voeg alle Exact Online administraties toe aan de selectie.',
        itgen_ct_selectdefaulttitle: 'Zet de selectie terug naar de laatst gebruikte administratie in Exact Online.',
        itgen_ct_helpbutton: 'Help',
        itgen_ct_helpbuttontt: 'Klik hier voor aanvullende hulp.',
        itgen_ct_first_import_date: 'Datum eerste import',
        itgen_ct_last_import_date: 'Datum laatste import',
        itgen_ct_total_imports: 'Totale imports',
        itgen_ct_total_rates_imported: 'Aantal ingelezen wisselkoersen',
        itgen_ct_total_duration: 'Totale duur',
        itgen_ct_statistics: 'Statistieken',
        itgen_ct_seconds: 'seconden',
        itgen_ct_copied: 'Succesvol naar klembord gekopieerd.',
        itgen_ct_copy: 'Kopieer naar klembord.',
        itgen_ct_form_division: 'Er moet minimaal één administratie worden geselecteerd. Voeg alstublieft één of meer administraties toe aan uw selectie.',
        itgen_ct_form_currency: "Er moet minimaal één valuta worden geselecteerd. Voeg alstublieft één of meer valuta's toe aan uw selectie.",
        itgen_ct_testrun: 'Testrun',
        itgen_ct_logo_text: 'Valuta Tools voor Exact Online Logo',
        itgen_ct_cloud_button: 'Probeer Exact Online met Power BI 180 dagen',
        itgen_ct_data_container_id: 'Data Container ID',
        itgen_ct_user_settings: 'Gebruikersinstellingen',
        itgen_ct_attention_cloud: 'Attentie!',
        itgen_ct_contact_nl: 'Nederland',
        itgen_ct_contact_sales: 'Verkoop',
        itgen_ct_contact_support: 'Ondersteuning',
        itgen_ct_contact_office_hours: 'Kantooruren',
        itgen_ct_support_portal: 'Support Portaal',
        itgen_ct_attention_cloud_text: 'Bereid uzelf tijdig voor op de overgang in 2022 van Valuta Tools naar Invantive Cloud.',
        itgen_ct_attention_cloud_steps: 'Lees hier de stappen.',
        itgen_ct_attention_jquery: 'jQuery is uitgeschakeld door software of beheerder. Vraag uw IT-beheerder om het gebruik van jQuery mogelijk te maken om met Valuta Tools te werken.',
        itgen_ct_contact_email_label: 'E-mail',
        itgen_ct_contact_email_url: 'info@invantive.nl',
        itgen_ct_contact_web_url: 'https://invantive.nl',
        itgen_ct_contact_web_label: 'invantive.nl',
        itgen_ct_contact_web: 'Web',
        itgen_ct_contact_heading: 'Contact',
        itgen_ct_contact_button: 'Contact'
    });
}]);

$invantive.configureAngular(app);

//
// Choose tool and proceed to next step.
//
$( ".bsrCurrency" ).click(function() {
     $('.slideIn').slideToggle();
     $('.slideIn2').slideToggle(200);
});

$( ".bsrBack" ).click(function() {
    location.reload();
});

$( ".closeError" ).click(function() {
    $(this).parent().fadeOut();
    $(this).parent().siblings('.errorOverlay').fadeOut();
});

$( ".contact" ).click(function() {
    $('.contactPrompt').fadeToggle();
    $('#background').animate({
        scrollTop: $('.contactPrompt').offset().top
    }, 2000);
});

$( ".closePortal" ).click(function() {
    $('.contactPrompt').fadeToggle();
});

$( ".userInfoWrapper" ).click(function() {
    $('.mySettings').fadeToggle();
    $('.settingsOverlay').fadeToggle();
});

$( '.settingsOverlay' ).click(function() {
    $('.mySettings').fadeToggle();
    $('.settingsOverlay').fadeToggle();
});

$( '.closeSettings' ).click(function() {
    $('.mySettings').fadeToggle();
    $('.settingsOverlay').fadeToggle();
});

$('#beforeDate').datepicker
( 
  { 
    dateFormat: 'dd-mm-yy'
  }
);
