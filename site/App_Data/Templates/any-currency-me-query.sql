--
-- Update user information after log on.
--
declare
  l_data_container_id varchar2 := sys_context('USERENV', 'DATA_CONTAINER_ID', 'eol');
  l_user_id_eol       varchar2 := sys_context('USERENV', 'USER_ID', 'eol');
  l_log_on_code       varchar2 := sys_context('USERENV', 'CURRENT_USER', 'eol');
  l_usr_id            integer;
  l_country           varchar2;
begin
  select min(usr.id)
  into   l_usr_id
  from   vt_users@valutatools usr
  where  usr.data_container_id = l_data_container_id
  and    usr.user_id           = l_user_id_eol
  ;
  if l_usr_id is null
  then
    l_country := :country;
    --
    select coalesce(usr_id_max, 0) + 1
    into   l_usr_id
    from   ( select max(usr.id) usr_id_max
             from   vt_users@valutatools usr
           )
    ;
    dbms_trace.put_line
    ( 'Register new user ID '
      || to_char(l_usr_id)
      || ' with Exact User ID '
      || l_user_id_eol
      || ' on data container ID '
      || l_data_container_id
      || '.'
    );
    insert into vt_users@valutatools
    ( id
    , uid
    , data_container_id
    , user_id
    , country
    , first_log_on_code
    , last_log_on_code
    , cnt_log_on
    , first_log_on_date
    , last_log_on_date
    , cnt_import
    , cnt_import_days
    , cnt_import_rates
    , cnt_import_divisions
    , last_duration_sec
    , total_duration_sec
    , first_import_date
    , last_import_date
    , email_address
    , company_name
    , display_language_code
    , last_number_of_days
    , last_divisions
    , last_currencies
    , customer_block_flag
    , payment_block_flag
    , visible_remark
    --
    , date_created
    , created_by
    , created_at
    , session_created
    , date_modified
    , modified_by
    , modified_at
    , session_modified
    , orig_system_group
    , orig_system_reference
    )
    select l_usr_id
    ,      newid()
    ,      l_data_container_id
    ,      l_user_id_eol
    ,      l_country
    ,      l_log_on_code
    ,      l_log_on_code
    ,      cast(0 as int64) cnt_log_on
    ,      to_date(sys_context('USERENV', 'USER_LAST_AUTHENTICATED', 'eol'), 'YYYYMMDDHH24MISS') first_log_on_date
    ,      to_date(sys_context('USERENV', 'USER_LAST_AUTHENTICATED', 'eol'), 'YYYYMMDDHH24MISS') last_log_on_date
    ,      cast(0 as int64) cnt_import
    ,      cast(0 as int64) cnt_import_days
    ,      cast(0 as int64) cnt_import_rates
    ,      cast(0 as int64) cnt_import_divisions
    ,      cast(0 as int64) last_duration_sec
    ,      cast(0 as int64) total_duration_sec
    ,      null first_import_date
    ,      null last_import_date
    ,      me.Email
    ,      me.UserCustomerDescription
    ,      me.LanguageCode
    ,      cast(7 as int64)
           last_number_of_days
    ,      null
           last_divisions
    --
    -- Most commonly used currencies.
    --
    ,      'EUR,GBP,USD,AUD,DKK,SEK,NOK,PLN'
           last_currencies
    ,      false
    ,      false
    ,      null
    --
    ,      sysdateutc
    ,      l_log_on_code
    ,      sys_context('USERENV', 'APPLICATION_FULL', 'eol')
    ,      sys_context('USERENV', 'SESSIONID', 'eol')
    ,      sysdateutc
    ,      l_log_on_code
    ,      sys_context('USERENV', 'APPLICATION_FULL', 'eol')
    ,      sys_context('USERENV', 'SESSIONID', 'eol')
    ,      null
    ,      null
    from   me@eol
    ;
    insert into auditevents@datadictionary
    ( message_code
    , user_message
    , last_natural_key
    , application_name
    , gui_action
    , gui_module
    , reference_key
    , reference_table_code
    , partition
    , provider_name
    , gui_module_name
    , gui_module_version
    , dimension1_name
    , dimension1_value
    , dimension2_name
    , dimension2_value
    , dimension3_name
    , dimension3_value
    , dimension4_name
    , dimension4_value
    , dimension5_name
    , dimension5_value
    , dimension6_name
    , dimension6_value
    , dimension7_name
    , dimension7_value
    , dimension8_name
    , dimension8_value
    , dimension9_name
    , dimension9_value
    , dimension10_name
    , dimension10_value
    , dimension11_name
    , dimension11_value
    )
    select 'xxvt003' message_code
    ,      'Registered valuta-tools.nl user '
           || t.FullName
           || case
              when t.Email is not null
              then ' with email address '
                   || t.Email
              else ' without email address'
              end
           || ' (language '
           || t.LanguageCode
           || ', Exact Online country '
           || l_country
           || case
              when t.Phone is not null
              then ', phone '
                   || t.Phone
              end
           || case
              when t.Mobile is not null
              then ', mobile '
                   || t.Mobile
              end
           || ')'
           || ', organization '
           || t.UserCustomerDescription
           || case
              when t.ChamberOfCommerceNumber is not null
              then ', CoC '
                   || t.ChamberOfCommerceNumber
              end
           || case
              when t.VATNumber is not null
              then ', VAT '
                   || t.VATNumber
              end
           || ', address '
           || t.AddressLine1
           || ' '
           || t.Country
           || '-'
           || t.Postcode
           || ' '
           || t.City
           || case
              when t.org_email is not null
              then ', email '
                   || t.org_email
              end
           || case
              when t.org_phone is not null
              then ', phone '
                   || t.org_phone
              end
           || case
              when t.org_website is not null
              then ', website '
                   || t.org_website
              end
           || '.'
           user_message
    ,      l_log_on_code
           last_natural_key
    ,      'valuta-tools.nl'
           application_name
    ,      'any-currency-me-query.sql'
           gui_action
    ,      'valuta-tools.nl 17.32'
           gui_module
    ,      null reference_key
    ,      null reference_table_code
    ,      null partition
    ,      'ExactOnlineAll' provider_name
    ,      'valuta-tools.nl' gui_module_name
    ,      l_country
           gui_module_version
    ,      'usr-full-name'
           dimension1_name
    ,      t.FullName
           dimension1_value
    ,      'usr-email-address'
           dimension2_name
    ,      t.Email
           dimension2_value
    ,      'usr-language'
           dimension3_name
    ,      sys_context('USERENV', 'UI_LANGUAGE_CODE', 'eol')
           dimension3_value
    ,      'usr-phone'
           dimension4_name
    ,      t.Phone
           dimension4_value
    ,      'usr-mobile'
           dimension5_name
    ,      t.Mobile
           dimension5_value
    ,      'org-name'
           dimension6_name
    ,      t.UserCustomerDescription
           dimension6_value
    ,      'org-vat-number'
           dimension7_name
    ,      t.VATNumber
           dimension7_value
    ,      'org-coc-number'
           dimension8_name
    ,      t.ChamberOfCommerceNumber
           dimension8_value
    ,      'org-phone'
           dimension9_name
    ,      t.org_phone
           dimension9_value
    ,      'org-web'
           dimension10_name
    ,      t.org_website
           dimension10_value
    ,      'user-ip-ext'
           dimension11_name
    ,      sys_context('USERENV', 'CLIENT_IP_ADDRESS_EXTERNAL', 'eol')
           dimension11_value
    --
    -- Find best matching organization.
    --
    from   ( select me.UserCustomerCode
             ,      me.UserCustomerDescription
             ,      me.FullName
             ,      me.Email
             ,      me.LanguageCode
             ,      me.Phone
             ,      me.Mobile
             ,      case
                    when me.UserCustomerDescription = sdn.Description
                    then 0
                    else 1 + levenshtein(me.UserCustomerDescription, sdn.Description) 
                    end 
                    char_distance
             ,      sdn.ChamberOfCommerceNumber
             ,      sdn.VATNumber
             ,      sdn.addressline1
             ,      sdn.postcode
             ,      sdn.city
             ,      sdn.country
             ,      sdn.email org_email
             ,      sdn.phone org_phone
             ,      sdn.website org_website
             from   me@eol me
             left
             outer
             join   systemdivisions@eol sdn
             on     sdn.customercode = me.usercustomercode
             and    sdn.status = 1
             and    levenshtein(me.UserCustomerDescription, sdn.Description) < length(me.UserCustomerDescription) * 0.5
           )
           t
    order
    by     char_distance
    limit  1
    ;
  end if;
  --
  update vt_users@valutatools
  set    last_log_on_date = sysdateutc
  ,      cnt_log_on       = cnt_log_on + 1
  ,      last_log_on_code = l_log_on_code
  where  id               = l_usr_id
  ;
  --
  if sqlrowcount = 0
  then
    raise_application_error
    ( 'xxvt004'
    , 'Could not update information for Exact Online user '''
      || l_user_id_eol
      || ''' on data container ID '''
      || l_data_container_id
      || '''.'
    );
  end if;
end;

select /*+ result_set_name('me') */
       me.ThumbnailPictureFormat
,      me.FullName
,      me.DivisionCustomerName
,      me.Email
,      me.LanguageCode
,      me.DataContainerID
,      me.UserID
,      me.Application
,      me.Phone
,      me.Mobile
,      stg.oer_last_day_loaded
,      stg.ecb_last_day_loaded
,      stg.oer_next_age
,      stg.ecb_next_age
,      substr
       ( to_char(stg.oer_last_day_loaded, 'DD-MM-YYYY HH24:MI:SS')
       , 1
       , 16)
       || ' (UTC); '
       || to_char(round(stg.oer_working_days_age, 1))
       || ' d.'
       oer_last_day_loaded_c
,      substr
       ( to_char(stg.ecb_last_day_loaded, 'DD-MM-YYYY HH24:MI:SS')
       , 1
       , 16
       )
       || ' (UTC); '
       || to_char(round(stg.ecb_working_days_age, 1))
       || ' d.'
       ecb_last_day_loaded_c
,      stg.ecb_working_days_age
,      stg.oer_working_days_age
,      me.ThumbnailPicture
from   exactonlinerest..me@eol me
join   (  select stg.oer_last_day_loaded
          ,      stg.ecb_last_day_loaded
          ,      case
                 when stg.ecb_use_flag
                 then sysdateutc
                      - stg.ecb_last_day_loaded
                      --
                      -- Subtract saturday/sunday for whole weeks.
                      --
                      - 2 * floor((sysdateutc - stg.ecb_last_day_loaded) / 7)
                      --
                      -- When partial week, then remove saturday/sunday when there was a weekend in the range.
                      --
                      - case
                      when to_number(to_char(stg.ecb_last_day_loaded, 'D')) > to_number(to_char(sysdateutc, 'D'))
                      then 2
                      --
                      -- Same week in the weekend.
                      --
                      when to_number(to_char(sysdateutc, 'D')) = 7 and floor(sysdateutc - stg.ecb_last_day_loaded) <= 2
                      then 2
                      when to_number(to_char(sysdateutc, 'D')) = 6 and floor(sysdateutc - stg.ecb_last_day_loaded) <= 2
                      then 1
                      else 0
                      end
                 else null
                 end
                 ecb_working_days_age
          ,      sysdateutc
                 -
                 case
                 when stg.ecb_use_flag
                 then trunc(sysdateutc + 1)
                      + 14/24
                      --
                      -- Time difference between ECB and UTC, excluding DST.
                      --
                      - 2/24
                      --
                      -- ECB has no rates in the weekend.
                      --
                      + case
                        when to_number(to_char(sysdateutc, 'D')) in (5, 6)
                        then 2
                        else 0
                        end
                 else null
                 end
                 ecb_next_age
          ,      case
                 when stg.oer_use_flag
                 then sysdateutc
                      - stg.oer_last_day_loaded
                      --
                      -- No correction for weekends. OER provides a continuous exchange rate.
                      --
                 else null
                 end
                 oer_working_days_age
          ,      sysdateutc
                 -
                 case
                 when stg.oer_use_flag
                 then trunc(sysdateutc + 1) + 1/24
                 else null
                 end
                 oer_next_age
          from   vt_settings@valutatools stg
      ) stg

select /*+ result_set_name('mestats') */
       vtu.first_import_date
       first_import_date
,      vtu.last_import_date
       last_import_date
,      vtu.cnt_import
       cnt_import
,      vtu.first_import_date
       first_import_date
,      vtu.cnt_import_rates
       cnt_import_rates
,      vtu.total_duration_sec
       total_duration_sec
,      me.DivisionCustomerName
,      me.FullName
,      me.ThumbnailPicture
,      vtu.last_number_of_days
       last_number_of_days
,      vtu.last_divisions
       last_divisions
,      vtu.last_currencies
       last_currencies
,      vtu.cnt_import_days
       cnt_import_days
,      vtu.visible_remark
       visible_remark
,      vtu.customer_block_flag
       customer_block_flag
,      vtu.payment_block_flag
       payment_block_flag
,      case       
       when vtu.customer_block_flag = true
       then ' Account is temporarily blocked.'
       else null
       end
       || case
          when vtu.payment_block_flag = true
          then ' Account blocked due to overdue payment.'
          else null
          end
       || case
          when vtu.visible_remark is not null
          then ' ' || vtu.visible_remark
          else null
          end
       remarks_text_contents
--
-- Color license text in header red when there is an issue.
--
,      case
       when vtu.customer_block_flag = true
            or vtu.payment_block_flag = true
            or vtu.visible_remark is not null
       then '#e63737'
       else '#6b6b6b'
       end
       remarks_text_color
from   exactonlinerest..me@eol me
join   vt_users@valutatools vtu
on     vtu.user_id           = me.UserID
and    vtu.data_container_id = sys_context('USERENV', 'DATA_CONTAINER_ID', 'eol')