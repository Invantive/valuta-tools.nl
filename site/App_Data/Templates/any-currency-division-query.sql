--
-- Display only system partitions that are accessible based on the license.
--
select /*+ result_set_name('partitions') */
       spn.label
,      spn.code
from   systempartitions@datadictionary spn
where  spn.provider_data_container_alias = 'eol'
order
by     spn.label