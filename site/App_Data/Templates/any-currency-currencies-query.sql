use
select min(sdn.code)
from   systempartitions@datadictionary spn
join   ( select /*+ http_disk_cache(false) http_memory_cache(true, true, interval '1 hour') */ 
                to_char(sdn.code) code_c
         ,      sdn.code
         ,      sdn.currency 
         from   systemdivisions@eol sdn 
       ) sdn
on     sdn.code_c = spn.code
where  spn.is_selected = true
and    spn.provider_data_container_alias = 'eol'

select /*+ http_disk_cache(true, true, interval '7 days') result_set_name('currencies') */
       cry.code
       code
,      cry.code || ' - ' || cry.description
       label
from   currencies@eol cry