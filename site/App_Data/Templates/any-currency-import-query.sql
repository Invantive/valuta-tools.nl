--
-- Load exchange rates from ECB and/or Open Exchange Rates
-- into Exact Online.
--
-- Expected runtime for 7 calendar days of new exchange rate
-- data across period of 14 calendar days, 200 companies:
-- TBD minutes.
--
-- Do not use memory or disk cache for Exact Online to
-- ensure current numbers.
--
begin
  set use-http-memory-cache@eol false
  ;
  set use-http-disk-cache@eol false
  ;
  create or replace table start@inmemorystorage
  as
  select sysdateutc start
  ;
end;

--
-- Perform checks on the parameters.
--
declare
  l_customer_block_flag boolean;
  l_payment_block_flag  boolean;
begin
  dbms_trace.put_line('Perform parameter validations.');
  if coalesce(length(:divisions), 0) = 0
  then
    raise_application_error('xxvt005', 'At least one company must be selected. Please add one or more companies to your selection.');
  end if;
  --
  if coalesce(length(:currencies), 0) = 0
  then
    raise_application_error('xxvt006', 'At least one currency must be selected. Please add one or more currencies to your selection.');
  end if;
  --
  if :number_of_days is null
  then
    raise_application_error('xxvt007', 'Number of days to load is missing. Please provide number of days to load.');
  end if;
  --
  if coalesce(length(regexp_replace(:number_of_days, '[0-9]', '')), 0) != 0
  then
    raise_application_error('xxvt013', 'Number of days to load is not a number. Please provide number of days to load.');
  end if;
  --
  if :number_of_days <= 0
  then
    raise_application_error('xxvt008', 'Number of days to load must be positive. Please provide a positive number of days to load.');
  end if;
  --
  if :testrun not in ('false', 'true')
  then
    raise_application_error('xxvt009', 'Test run must be either true or false. Please provide a value.');
  end if;
  --
  if :reference_date is not null
  then
    if :reference_date not like '20__-__-__'
    then
      raise_application_error('xxvt010', 'Reference date ''' || :reference_date || ''' does not match the date mask ''YYYY-MM-DD''.');
    end if;
    --
    if coalesce(length(regexp_replace(:reference_date, '^20[0-9][0-9]-(01|02|03|04|05|06|07|08|09|10|11|12)-[0-3][0-9]$', '')), 0) != 0
    then
      raise_application_error('xxvt011', 'Reference date ''' || :reference_date || ''' does not match the date mask ''YYYY-MM-DD''.');
    end if;
    --
    if to_date(:reference_date, 'YYYY-MM-DD') > trunc(sysdateutc)
    then
      raise_application_error('xxvt012', 'Reference date ''' || :reference_date || ''' must be in the past or today.');
    end if;
  end if;
  --
  -- Check for blocks.
  --
  select vtu.customer_block_flag
  ,      vtu.payment_block_flag
  into   l_customer_block_flag
  ,      l_payment_block_flag
  from   exactonlinerest..me@eol me
  join   vt_users@valutatools vtu
  on     vtu.user_id           = me.UserID
  and    vtu.data_container_id = sys_context('USERENV', 'DATA_CONTAINER_ID', 'eol')
  ;
  if l_payment_block_flag
  then
    raise_application_error
    ( 'xxvt013'
    , 'Import blocked due to overdue payment. Please contact Invantive Finance and quote ID ''' 
      || sys_context('USERENV', 'DATA_CONTAINER_ID', 'eol') 
      || '''.'
    );
  end if;
  if l_customer_block_flag
  then
    raise_application_error
    ( 'xxvt014'
    , 'Import blocked due to block on your organization. Please contact Invantive Finance and quote ID '''
      || sys_context('USERENV', 'DATA_CONTAINER_ID', 'eol') 
      || '''.'
    );
  end if;
end;

--
-- Select division(s).
--
-- On Data Access Point currently only one division is selected.
-- However, you can select for instace also multiple or all divisions and load
-- the exchange rates in parallel, even when division have different
-- base currencies.
--
-- Archived companies are excluded.
--
use
select spn.code
,      spn.provider_data_container_alias
from   systempartitions@DataDictionary spn
join   string_split(:divisions, ',') x
on     x.value = spn.code
where  spn.provider_data_container_alias = 'eol'
and    spn.is_active = true

create or replace table divisioncurrencies@inmemorystorage
as
select spn.short_name
,      spn.name
,      spn.code     division_code_selected
,      sdn.code     division_id
,      sdn.currency division_currency
,      spn.subscription_holder_code
,      spn.subscription_holder_name
,      spn.company_name
,      spn.company_address
,      spn.company_city
,      spn.company_state
,      spn.company_zip_code
,      spn.company_country
,      spn.company_email_address
,      spn.company_phone
,      spn.company_web_site
,      spn.company_coc_number
,      spn.company_vat_number
from   systempartitions@datadictionary spn
join   ( select /*+ http_memory_cache(true, true, interval '1 hour') */
                to_char(sdn.code) code_c
         ,      sdn.code
         ,      sdn.currency
         from   systemdivisions@eol sdn
       ) sdn
on     sdn.code_c = spn.code
where  spn.is_selected = true
and    spn.provider_data_container_alias = 'eol'

--
-- Exact Online only supports a limited list of pre-defined currencies.
-- Take a sample from any division to avoid loading rates for currencies
-- not supported by Exact Online.
--
create or replace table currencytypes@inmemorystorage
as
select /*+ http_disk_cache(true, true, interval '7 days') */
       distinct
       cry.code
from   exactonlinerest..currencies@eol cry
--
-- Filter on selected currencies in user interface.
--
join   string_split(:currencies, ',') x
on     x.value = cry.code
where  cry.division
       =
       ( select min(division_id)
         from   divisioncurrencies@inmemorystorage
       )
union
select dcy.division_currency code
from   divisioncurrencies@inmemorystorage dcy

--
-- For Exact Online companies in Euros, we will use ECB as the preferred source
-- when ECB provides the exchange rates (approximately 30 currencies).
-- They are completed with exchange rates
-- from Open Exchange Rates for the currencies not provided by ECB.
--
-- For non-Euro companies in Exact Online, ECB will only be used for the exchange
-- rate with the Euro. Open Exchange Rates will be the source for all other currencies.
--
create or replace table ecbpreferredcurrencies@inmemorystorage
as
select t.value currency_code
from   string_split('EUR,AUD,BGN,BRL,CAD,CHF,CNY,CZK,DKK,GBP,HKD,HRK,HUF,IDR,ILS,INR,ISK,JPY,KRW,MXN,MYR,NOK,NZD,PHP,PLN,RON,RUB,SEK,SGD,THB,TRY,USD,ZAR', ',') t

--
-- Exact is not exact, uses double floating point precision.
-- Rate is derived from two sources:
-- * for ECB rates from central rate ("spilkoers"), not from rate on
--   two separate currency combination.
-- * for USD/xxx rates excluding EUR: from Open Exchange Rates latest known rate.
--
-- For ECB: last completed business day, which usually completed around 16:00 CET on
-- every working days, excluding days on which the central settlement system
-- TARGET for EURO payments is closed.
--
create or replace table allrates@inmemorystorage
as
select ere.rate_source
,      case
       when pwr.pwr = 1
       then ere.source_currency
       else ere.target_currency
       end
       source_currency
       label 'Source Currency'
,      case
       when pwr.pwr = 1
       then ere.target_currency
       else ere.source_currency
       end
       target_currency
       label 'Target Currency'
,      ere.time_day label 'Day'
,      power(ere.rate, pwr.pwr) rate label 'Rate'
,      case
       when pwr.pwr != -1
       then true
       else false
       end
       derived_flag
,      to_char(ere.time_day, 'YYYYMMDD')
       || '-'
       || case when pwr.pwr = 1 then ere.source_currency else ere.target_currency end
       || '-'
       || case when pwr.pwr = 1 then ere.target_currency else ere.source_currency end
       nk
       label 'Natural Key'
,      pwr.pwr label 'Direction'
from   ( select 1 pwr union select -1 ) pwr
join   vt_rates@valutatools ere
on     ere.time_day
       >=
       --
       -- Always select at least one day with actual data
       -- in the range to load.
       --
       -- When it is Monday and ECB was last loaded on Friday,
       -- we will include all exchange rates starting Friday for
       -- both OER and ECB.
       --
       ( select /*+ low_cost */
                min(t.time_day)
         from   ( select oer_last_day_loaded time_day
                  from   vt_settings@valutatools
                  where  oer_use_flag = true
                  union all
                  select ecb_last_day_loaded time_day
                  from   vt_settings@valutatools
                  where  ecb_use_flag = true
                  union all
                  select case
                         when :reference_date is not null
                         then to_date(:reference_date, 'YYYY-MM-DD')
                         else trunc(sysdateutc)
                         end
                         - :number_of_days time_day
                  from   vt_settings@valutatools
                ) t
       )
and    ere.time_day
       <=
       case
       when :reference_date is not null
       then to_date(:reference_date, 'YYYY-MM-DD')
       else trunc(sysdateutc)
       end
where  1=1
and    ere.source_currency != ere.target_currency
--
-- Restrict to the currencies supported by Exact Online.
--
and    ere.source_currency in ( select code from currencytypes@inmemorystorage )
and    ere.target_currency in ( select code from currencytypes@inmemorystorage )
--
-- Only include rates for which the target is used
-- as currency in any company to be loaded.
--
-- This saves at least 75% in terms of rows.
--
and    case
       when pwr.pwr = 1
       then ere.target_currency
       else ere.source_currency
       end
       in
       ( select distinct division_currency
         from   divisioncurrencies@inmemorystorage
       )
and    ( ( ere.rate_source = 'ECB'
           and true = ( select /*+ low_cost */ ecb_use_flag from vt_settings@valutatools )
         )
         or
         ( ere.rate_source = 'OER'
           and true = ( select /*+ low_cost */ oer_use_flag from vt_settings@valutatools )
         )
       )

--
-- Prefer to take EUR exchange rates only from ECB. All USD and GBP rates come from OER.
--
create or replace table rates@InMemoryStorage
as
select *
from   allrates@inmemorystorage
where  rate_source='ECB'
union  distinct
       on
       nk
select *
from   allrates@inmemorystorage
where  rate_source='OER'
and    derived_flag = false
union  distinct
       on
       nk
select *
from   allrates@inmemorystorage
where  rate_source='OER'
and    derived_flag = true

create or replace table oldexchangerates@inmemorystorage
as
select /*+ http_disk_cache(false) http_memory_cache(false) */
       to_char(exe.division)
       || '-'
       || to_char(exe.startdate, 'YYYYMMDD')
       || '-'
       || exe.sourcecurrency
       || '-'
       || exe.targetcurrency
       nk
,      exe.division
,      exe.DivisionShortName
,      exe.DivisionName
,      exe.startdate
,      exe.sourcecurrency
,      exe.targetcurrency
,      exe.rate
,      exe.Created
,      exe.Modified
,      exe.CreatorFullName
,      exe.ModifierFullName
from   exactonlinerest..exchangerates@eol exe
--
-- Query Exact for exchange rates in the date range being loaded.
--
-- The query on inmemorystorage is a low-cost operation, so it will be performed
-- first, leading to a minimum and maximum range of dates. These constants will be forwarded
-- to the Exact Online API as a filter on startdate.
-- Using an 'in' is possible too, but Exact Online API needs per call approximately 350 ms for
-- 60 dates with an IN clause compared to 150 ms for a date range with minimum/maximum.
--
where  exe.startdate >= ( select /*+ low_cost */ min(ered.time_day) from rates@inmemorystorage ered )
and    exe.startdate <= ( select /*+ low_cost */ max(ered.time_day) from rates@inmemorystorage ered )

create or replace table newexchangerates@inmemorystorage
as
select dcy.division_id
,      dcy.short_name
       division_short_name
,      dcy.name
       division_name
,      ere.rate
,      ere.source_currency
,      ere.target_currency
,      ere.time_day
,      dcy.division_code_selected
       || '-'
       || ere.nk
       nk
--
-- Create cartesian product of selected Exact Online divisions
--
from   divisioncurrencies@inmemorystorage dcy
join   rates@inmemorystorage ere
--
-- Choose the right conversion direction per Exact Online company on hand.
--
on     ere.target_currency = dcy.division_currency
--
-- Exclude exchange rates for any date which already has at least one exchange rate already loaded.
--
and    ( select listagg('###' || exe.nk || '###' , '')
         from   oldexchangerates@inmemorystorage exe
       )
       not like
       concat('%###', dcy.division_code_selected, '-', ere.nk, '###%')
order
by     ere.nk asc

--
-- Load new exchange rates.
--
begin
  if cast(:testrun as boolean) = false
  then
    dbms_trace.put_line('Load exchange rates.');
    bulk insert into exactonlinerest..ExchangeRates@eol
    ( division
    , rate
    , sourcecurrency
    , targetcurrency
    , startdate
    )
    select t.division_id
    ,      t.rate
    ,      t.source_currency
    ,      t.target_currency
    ,      t.time_day
    from   newexchangerates@inmemorystorage t
    ;
  else
    dbms_trace.put_line('Skip load of exchange rates.');
  end if;
end;

--
-- Compose report contents.
--
select /*+ result_set_name('importstats') */
       t1.cnt_rates_loaded
,      t1.cnt_divisions_loaded
,      t2.cnt_divisions_selected
,      t2.cnt_distinct_base_currencies
,      t2.distinct_base_currencies
from   ( select count(*)
                cnt_rates_loaded
                label 'Exchange rates loaded'
         ,      count(distinct division_id)
                cnt_divisions_loaded
                label 'Companies changed'
         from   newexchangerates@inmemorystorage
       ) t1
join   ( select count(division_code_selected)
                cnt_divisions_selected
         ,      count(distinct division_currency)
                cnt_distinct_base_currencies
         ,      listagg(distinct division_currency, ',')
                distinct_base_currencies
         from   divisioncurrencies@inmemorystorage
       ) t2

select /*+ result_set_name('importcompanies') */
       t.subscription_holder_code
,      t.subscription_holder_name
,      t.short_name
,      t.name
,      t.division_currency
,      t.division_id
,      t.company_name
,      t.company_address
,      t.company_city
,      t.company_state
,      t.company_zip_code
,      t.company_country
,      t.company_email_address
,      t.company_phone
,      t.company_web_site
,      t.company_coc_number
,      t.company_vat_number
from   divisioncurrencies@inmemorystorage t
order
by     t.subscription_holder_code
,      t.short_name

select /*+ result_set_name('importdetails') */
       t.division_short_name
,      t.division_name
,      t.time_day
,      t.source_currency
,      t.target_currency
,      t.rate
,      t.nk
from   newexchangerates@inmemorystorage t
order
by     t.division_short_name
,      t.time_day
,      t.source_currency
,      t.target_currency

select /*+ result_set_name('importoldrates') */
       exe.DivisionShortName
,      exe.DivisionName
,      exe.startdate
,      exe.sourcecurrency
,      exe.targetcurrency
,      exe.rate
,      exe.Modified
,      exe.ModifierFullName
,      exe.Created
,      exe.CreatorFullName
,      exe.nk
from   oldexchangerates@inmemorystorage exe
order
by     exe.DivisionShortName
,      exe.startdate
,      exe.sourcecurrency
,      exe.targetcurrency

--
-- Register statistics.
--
declare
  l_data_container_id varchar2 := sys_context('USERENV', 'DATA_CONTAINER_ID', 'eol');
  l_user_id           varchar2 := sys_context('USERENV', 'USER_ID', 'eol');
  l_log_on_code       varchar2 := sys_context('USERENV', 'CURRENT_USER', 'eol');
  l_cnt_rates         integer;
  l_cnt_divisions     integer;
  l_duration_sec      integer;
  l_divisions         varchar2;
  l_currencies        varchar2;
  l_base_currencies   varchar2;
begin
  select listagg(value, ',')
  into   l_divisions
  from   (select distinct value from string_split(:divisions, ',') order by value)
  ;
  select listagg(value, ',')
  into   l_currencies
  from   (select distinct value from string_split(:currencies, ',') order by value)
  ;
  select listagg(division_currency, ',')
  into   l_base_currencies
  from   (select distinct division_currency from divisioncurrencies@inmemorystorage order by division_currency)
  ;
  select count(*)
         cnt_rates
  ,      count(distinct division_id)
         cnt_divisions
  into   l_cnt_rates
  ,      l_cnt_divisions
  from   newexchangerates@inmemorystorage
  ;
  select round((sysdateutc - start.start) * 86400)
  into   l_duration_sec
  from   start@inmemorystorage
  ;
  update vt_users@valutatools
  set    first_import_date     = coalesce(first_import_date, sysdateutc)
  ,      last_import_date      = sysdateutc
  ,      cnt_import            = cnt_import + 1
  ,      cnt_import_days       = cnt_import_days
                                 +
                                 case
                                 when trunc(sysdateutc) != trunc(last_import_date)
                                 then 1
                                 else 0
                                 end
  ,      cnt_import_rates      = cnt_import_rates + l_cnt_rates
  ,      cnt_import_divisions  = cnt_import_divisions + l_cnt_divisions
  ,      last_duration_sec     = l_duration_sec
  ,      total_duration_sec    = total_duration_sec + l_duration_sec
  --
  -- Memorize used settings.
  --
  -- testrun and reference_date are not memorized, since they are
  -- typical one-time use settings.
  --
  ,      last_number_of_days   = :number_of_days
  ,      last_divisions        = l_divisions
  ,      last_currencies       = l_currencies
  ,      last_base_currencies  = l_base_currencies
  --
  ,      date_modified         = sysdateutc
  ,      modified_by           = l_log_on_code
  ,      modified_at           = sys_context('USERENV', 'APPLICATION_FULL', 'eol')
  ,      session_modified      = sys_context('USERENV', 'SESSIONID', 'eol')
  where  data_container_id = l_data_container_id
  and    user_id           = l_user_id
  ;
  insert into vt_actions@valutatools
  ( uid
  , user_id
  , data_container_id
  , cnt_rates
  , cnt_divisions
  , duration_sec
  , number_of_days
  , divisions
  , currencies
  , base_currencies
  , date_occurrence
  --
  , date_created
  , created_by
  , created_at
  , session_created
  , date_modified
  , modified_by
  , modified_at
  , session_modified
  , orig_system_group
  , orig_system_reference
  )
  values
  ( newid()
  , l_user_id
  , l_data_container_id
  , l_cnt_rates
  , l_cnt_divisions
  , l_duration_sec
  , :number_of_days
  , l_divisions
  , l_currencies
  , l_base_currencies
  , sysdateutc
  --
  , sysdateutc
  , l_log_on_code
  , sys_context('USERENV', 'APPLICATION_FULL', 'eol')
  , sys_context('USERENV', 'SESSIONID', 'eol')
  , sysdateutc
  , l_log_on_code
  , sys_context('USERENV', 'APPLICATION_FULL', 'eol')
  , sys_context('USERENV', 'SESSIONID', 'eol')
  , null
  , null
  );
end;
