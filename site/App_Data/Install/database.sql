--
-- On full instance:
--
revoke create on schema public from public;

--
-- Per install:
--
create role tvalutatools_owner
with   nologin
       nosuperuser
       nocreatedb
       nocreaterole
       inherit
       noreplication
;
create role tvalutatools_user
with   nologin
       nosuperuser
       nocreatedb
       nocreaterole
       inherit
       noreplication
;
create role tvalutatools_reader
with   nologin
       nosuperuser
       nocreatedb
       nocreaterole
       inherit
       noreplication
;
--
-- For installation.
--
create user tvalutatools_install
with   login
       nosuperuser
       nocreatedb
       nocreaterole
       inherit
       noreplication
password '...'
;
grant tvalutatools_owner to tvalutatools_install
;
--
-- For daily use and change.
--
create user tvalutatools_web
with   login
       nosuperuser
       nocreatedb
       nocreaterole
       inherit
       noreplication
password '...'
;
grant tvalutatools_user to tvalutatools_web
;


create database tvalutatools
with   owner = postgres
       encoding = 'utf8'

alter database tvalutatools owner to tvalutatools_install

revoke all on database tvalutatools from public



grant connect on database tvalutatools to tvalutatools_owner;
grant connect on database tvalutatools to tvalutatools_reader;
grant connect on database tvalutatools to tvalutatools_user;

grant usage, create on schema public to tvalutatools_owner;
grant usage on schema public to tvalutatools_reader;
grant usage on schema public to tvalutatools_user;

--
-- For existing tables
--
grant all on all tables in schema public to tvalutatools_owner;
grant select, insert, update, delete on all tables in schema public to tvalutatools_user;
grant select on all tables in schema public to tvalutatools_reader;
grant usage on all sequences in schema public to tvalutatools_owner;
grant usage on all sequences in schema public to tvalutatools_user;

--
-- For new tables
--
alter default privileges in schema public grant all on tables to tvalutatools_owner;
alter default privileges in schema public grant select, insert, update, delete on tables to tvalutatools_user;
alter default privileges in schema public grant select on tables to tvalutatools_reader;
alter default privileges in schema public grant usage on sequences to tvalutatools_owner;
alter default privileges in schema public grant usage on sequences to tvalutatools_user;

--
-- Root access.
--
grant tvalutatools_owner to postgres;