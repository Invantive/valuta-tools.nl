drop table vt_settings@valutatools

--
-- Configurable settings.
--
-- Contains exactly 1 row.
--
-- Alias: stg.
--
create or replace table vt_settings@valutatools
( dummy                 varchar2(1)   -- not null
, ecb_last_day_loaded   datetime      -- not null
, ecb_use_flag          boolean       -- not null
, oer_last_day_loaded   datetime      -- not null
, oer_use_flag          boolean       -- not null
--
, date_created          datetime      -- not null
, created_by            varchar2(240) -- not null
, created_at            varchar2(240) -- not null
, session_created       varchar2(240) -- not null
, date_modified         datetime      -- not null
, modified_by           varchar2(240) -- not null
, modified_at           varchar2(240) -- not null
, session_modified      varchar2(240) -- not null
, orig_system_group     varchar2(240)
, orig_system_reference varchar2(240)
)

--
-- Execute natively on PostgreSQL or SQL Server.
--
create unique index vt_stg_nk on vt_settings(dummy)

create unique index vt_stg_uk_orig_system_reference on vt_settings(orig_system_reference)

--
-- Seed settings.
--
insert into vt_settings@valutatools
( dummy
, ecb_last_day_loaded
, ecb_use_flag
, oer_last_day_loaded
, oer_use_flag
, date_created
, created_by
, created_at
, session_created
, date_modified
, modified_by
, modified_at
, session_modified
)
select 'X'
,      add_months(trunc(sysdate, -2), -120)
,      true
,      add_months(trunc(sysdate, -2), -120)
,      false
,      sysdate date_created
,      user    created_by
,      sys_context('USERENV', 'APPLICATION_FULL') created_at
,      sys_context('USERENV', 'SESSIONID') session_created
,      sysdate date_modified
,      user    modified_by
,      sys_context('USERENV', 'APPLICATION_FULL') modified_at
,      sys_context('USERENV', 'SESSIONID') session_modified

--
-- Exact Online Users.
--
-- Contains 1 row per user with the data container ID
-- signaling the Exact Online country and the
-- user_id the ID of the user in division 1.
--
-- Alias: usr.
--
create or replace table vt_users@valutatools
( id                    number(15, 0)  not null
, uid                   guid           not null
, data_container_id     varchar2(240)  not null
, user_id               varchar2(240)  not null
, country               varchar2(2)    not null
, first_log_on_code     varchar2(240)  not null
, last_log_on_code      varchar2(240)  not null
, cnt_log_on            pls_integer    not null
, first_log_on_date     datetime       not null
, last_log_on_date      datetime       not null
, cnt_import            pls_integer    not null
, first_import_date     datetime       
, last_import_date      datetime       
, email_address         varchar2(240)  -- not null
, company_name          varchar2(240)  not null
, display_language_code varchar2(5)    not null
, cnt_import_rates      pls_integer    not null
, cnt_import_divisions  pls_integer    not null
, last_duration_sec     pls_integer    not null
, total_duration_sec    pls_integer    not null
, last_number_of_days   pls_integer        null
, last_divisions        varchar2(4000)     null
, last_currencies       varchar2(2000)     null
, last_base_currencies  varchar2(2000)     null
, cnt_import_days       pls_integer    not null
, customer_block_flag   boolean        not null
, payment_block_flag    boolean        not null
, visible_remark        varchar2(1000)     null
--
, date_created          datetime      not null
, created_by            varchar2(240) not null
, created_at            varchar2(240) not null
, session_created       varchar2(240) not null
, date_modified         datetime      not null
, modified_by           varchar2(240) not null
, modified_at           varchar2(240) not null
, session_modified      varchar2(240) not null
, orig_system_group     varchar2(240)     null
, orig_system_reference varchar2(240)     null
)

--
-- Execute natively on PostgreSQL or SQL Server.
--
create unique index vt_usr_nk on vt_users(data_container_id, user_id)

create unique index vt_usr_uk_orig_system_reference on vt_users(orig_system_reference)

--
-- User actions.
--
-- Alias: atn.
--
create or replace table vt_actions@valutatools
( uid                   guid           not null
, data_container_id     varchar2(240)  not null
, user_id               varchar2(240)  not null
, cnt_rates             pls_integer    not null
, cnt_divisions         pls_integer    not null
, duration_sec          pls_integer    not null
, number_of_days        pls_integer        null
, divisions             varchar2(4000)     null
, currencies            varchar2(2000)     null
, base_currencies       varchar2(2000)     null
, date_occurrence       datetime       not null
--
, date_created          datetime      not null
, created_by            varchar2(240) not null
, created_at            varchar2(240) not null
, session_created       varchar2(240) not null
, date_modified         datetime      not null
, modified_by           varchar2(240) not null
, modified_at           varchar2(240) not null
, session_modified      varchar2(240) not null
, orig_system_group     varchar2(240)     null
, orig_system_reference varchar2(240)     null
)

--
-- Execute natively on PostgreSQL or SQL Server.
--
create unique index vt_atn_nk on vt_actions(data_container_id, user_id, date_occurrence)

create unique index vt_atn_uk_orig_system_reference on vt_actions(orig_system_reference)

--
-- Exchange rates.
--
-- Note that currency codes such as VEF_BLKMKT have not 3 but 10 characters.
--
-- Alias: rte.
--
create or replace table vt_rates@valutatools
( rate_source           varchar2(3)    not null
, source_currency       varchar2(10)   not null
, target_currency       varchar2(10)   not null
, rate                  number(28, 10) not null
, time_day              datetime       not null
, uid                   guid           not null
--
, date_created          datetime       not null
, created_by            varchar2(240)  not null
, created_at            varchar2(240)  not null
, session_created       varchar2(240)  not null
, date_modified         datetime       not null
, modified_by           varchar2(240)  not null
, modified_at           varchar2(240)  not null
, session_modified      varchar2(240)  not null
, orig_system_group     varchar2(240)      null
, orig_system_reference varchar2(240)      null
)

--
-- Execute natively on PostgreSQL or SQL Server.
--
create unique index vt_rte_uk_1 on vt_rates(rate_source, source_currency, target_currency, time_day)

create unique index vt_rte_uk_orig_system_reference on vt_rates(orig_system_reference)

create index vt_rte_n1 on vt_rates(time_day, source_currency, target_currency)

--
-- Exact Online company configurations.
--
-- Alias: cpy.
--
create or replace table vt_companies@valutatools
( id                    number(15, 0) -- not null
, code                  int32         -- not null
, description           varchar2(240) -- not null
, uid                   guid          not null
--
, date_created          datetime      -- not null
, created_by            varchar2(240) -- not null
, created_at            varchar2(240) -- not null
, session_created       varchar2(240) -- not null
, date_modified         datetime      -- not null
, modified_by           varchar2(240) -- not null
, modified_at           varchar2(240) -- not null
, session_modified      varchar2(240) -- not null
, orig_system_group     varchar2(240)
, orig_system_reference varchar2(240)
)
