@echo on
rem
rem Update memorized exchange rates every so many time intervals.
rem
rem Run every day at UTC 05:10.
rem

set LOGFILE=d:\jobs\log\job0510\%date:~10,4%%date:~4,2%.log

echo *** >>%LOGFILE% 2>&1
echo *** Start job0510.bat %DATE% %TIME% *** >>%LOGFILE% 2>&1
echo *** >>%LOGFILE% 2>&1

call d:\jobs\batch\vt-load-exchange-rates-into-database.bat >>%LOGFILE% 2>&1

IF %ERRORLEVEL% NEQ 0 (
  echo *** Fail job0510.bat %DATE% %TIME% *** >>%LOGFILE% 2>&1
  exit /b %ERRORLEVEL%
)

echo *** End job0510.bat %DATE% %TIME% *** >>%LOGFILE% 2>&1
