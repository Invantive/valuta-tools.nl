local on error exit failure

local define AGREEMENT_CODE "L599595213"

local remark *** END OF CONFIGURATION SECTION ***

local on error continue

alter session set billing id '${AGREEMENT_CODE}'

declare
  --
  -- Number of days before today to load incrementally.
  -- Once a combination is loaded, it remains untouched.
  --
  l_number_of_days                 pls_integer := 7;
  --
  -- Precision of rates after decimal digit.
  --
  l_rate_dec_precision             pls_integer := 10;
  --
  l_time_day_oer_min               date;
  l_time_day_oer_max               date;
  l_time_day_vtl_min_in_load_range date;
  l_time_day_vtl_max_in_load_range date;
  l_ecb_last_day_loaded_utc        date;
  l_oer_last_day_loaded_utc        date;
  l_time_day_min                   date;
begin
  --
  -- Use disk cache for ECB exchange rates for one hour after downloading
  -- to reduce load on ECB in high traffic scenarios.
  --
  set use-http-disk-cache@ecb true
  ;
  set http-disk-cache-max-age-sec@ecb 3600
  ;
  --
  -- Use disk cache for Open Exchange Rates for one hour after downloading
  -- to reduce load on OER in high traffic scenarios.
  --
  set use-http-disk-cache@oer true
  ;
  set http-disk-cache-max-age-sec@oer 3600
  ;
  create or replace table basecurrencies@inmemorystorage
  as
  select 'EUR' currencycode
  union all
  select 'USD' currencycode
  union all
  select 'GBP' currencycode
  ;
  --
  -- From OER we download only exchange rates from the previous day
  -- since during the whole day new exchange rates are made available.
  --
  -- This is accomplished by using trunc(sysdateutc) minus the values from
  -- the range table function starting at 1.
  --
  -- Open Exchange Rates when queried on October 15, 4:00 AM with
  -- date October 14, 0:00 AM will return the exchange rates dated October 14, 23:59:59.
  --
  create or replace table oerrates@inmemorystorage
  as
  select trunc(hrt.timestamp_utc) time_day
  ,      hrt.timestamp_utc time_full_utc
  ,      hrt.base
  ,      hrt.name
  ,      hrt.rate
  from   range(l_number_of_days, 1)@datadictionary rge
  join   basecurrencies@InMemoryStorage bce
  join   HistoricalRatesByDateAndBase(trunc(sysdateutc) - rge.value, bce.currencycode)@oer hrt
  --
  -- The exchange rate of the Venezuelan Bolivar was 0 at the day
  -- it was succeeded by the Bolivar Soberano.
  --
  on     hrt.rate != 0
  ;
  create or replace table ratestoload@inmemorystorage
  as
  --
  -- Reverse conversion: from EUR to foreign currency.
  -- Last 1..90 calendar days.
  --
  select 'ECB'
         rate_source
  ,      'EUR'
         source_currency
         label 'Source Currency'
  ,      ecb.currency
         target_currency
         label 'Target Currency'
  ,      ecb.rate     
         rate
  ,      trunc(ecb.time)
         time_day
         label 'Day'
  ,      ( trunc(ecb.time)
           --
           -- Only date is provided, but ECB supplies data around 16:00,
           -- based upon reconciliation around 14:15.
           -- Explicit correction.
           --
           + 14/24
         )
         --
         -- Is probably in CET. Correct to UTC.
         -- Does not compensate yet for DST.
         --
         - 2/24
         time_full_utc
  from   vt_settings@valutatools stg
  join   ecb_euro_exchange_rates_last_90_days@ecb ecb
  on     trunc(ecb.time) >= trunc(sysdateutc) - l_number_of_days
  where  stg.ecb_use_flag = true
  --
  -- Open Exchange Rates.
  --
  union all
  select 'OER' rate_source
  ,      oer.base  source_currency
  ,      oer.name  target_currency
  ,      oer.rate  rate
  ,      oer.time_day
  ,      oer.time_full_utc
  from   vt_settings@valutatools stg
  join   oerrates@inmemorystorage oer
  where  stg.oer_use_flag = true
  ;
  select min(rtesoll.time_day)
  into   l_time_day_min
  from   ratestoload@inmemorystorage rtesoll
  ;
  --
  -- Load the rest while checking whether
  -- they are already registerd.
  --
  bulk insert into vt_rates@valutatools
  ( uid
  , rate_source
  , source_currency
  , target_currency
  , rate
  , time_day
  , date_created
  , created_by
  , created_at
  , session_created
  , date_modified
  , modified_by
  , modified_at
  , session_modified
  , orig_system_reference
  )
  select newid()
  ,      rtesoll.rate_source
  ,      rtesoll.source_currency
  ,      rtesoll.target_currency
  ,      round(rtesoll.rate, l_rate_dec_precision)
  ,      rtesoll.time_day
  ,      sysdateutc date_created
  ,      sys_context('USERENV', 'CURRENT_USER', 'valutatools') created_by
  ,      sys_context('USERENV', 'APPLICATION_FULL', 'valutatools') created_at
  ,      sys_context('USERENV', 'SESSIONID', 'valutatools') session_created
  ,      sysdateutc date_modified
  ,      sys_context('USERENV', 'CURRENT_USER', 'valutatools') modified_by
  ,      sys_context('USERENV', 'APPLICATION_FULL', 'valutatools') modified_at
  ,      sys_context('USERENV', 'SESSIONID', 'valutatools') session_modified
  --
  -- SQL Server requires orig_system_reference to be unique include null.
  --
  ,      rtesoll.rate_source
         || '-'
         || rtesoll.source_currency
         || '-'
         || rtesoll.target_currency
         || '-'
         || rtesoll.time_day
  from   ratestoload@inmemorystorage rtesoll
  left
  outer
  join   ( select *
           from   vt_rates@valutatools rteist
           where  /* Reduce data set to consider. */
                  rteist.time_day >= l_time_day_min
         )
         rteist
  on     rteist.rate_source     = rtesoll.rate_source
  and    rteist.source_currency = rtesoll.source_currency
  and    rteist.target_currency = rtesoll.target_currency
  and    rteist.time_day        = rtesoll.time_day
  --
  -- Load only where not present.
  --
  where  rteist.rate_source is null
  ;
  --
  -- Register last available date of exchange rates per rate source.
  -- ECB only provides exchange rates on some days.
  --
  select max(case when rtd.rate_source = 'OER' then rtd.time_full_utc else null end) oer_last_day_loaded_utc
  ,      max(case when rtd.rate_source = 'ECB' then rtd.time_full_utc else null end) ecb_last_day_loaded_utc
  into   l_oer_last_day_loaded_utc
  ,      l_ecb_last_day_loaded_utc
  from   ratestoload@inmemorystorage rtd
  ;
  update vt_settings@valutatools
  set    ecb_last_day_loaded
         =
         case
         when ecb_use_flag = true
              and l_ecb_last_day_loaded_utc >= ecb_last_day_loaded
         then l_ecb_last_day_loaded_utc
         else ecb_last_day_loaded
         end
  ,      oer_last_day_loaded
         =
         case
         when oer_use_flag = true
              and l_oer_last_day_loaded_utc >= oer_last_day_loaded
         then l_oer_last_day_loaded_utc
         else oer_last_day_loaded
         end
  ,      date_modified    = sysdateutc
  ,      modified_by      = sys_context('USERENV', 'CURRENT_USER', 'valutatools')
  ,      modified_at      = sys_context('USERENV', 'APPLICATION_FULL', 'valutatools')
  ,      session_modified = sys_context('USERENV', 'SESSIONID', 'valutatools')
  ;
end;

insert into auditevents@DataDictionary
( is_monitoring
, metric_name
, service_name
, message_code
, user_message
, metric_value
, unit_of_measure
)
values
( true
, 'LoadDatabaseExchangeRatesDuration'
, 'valuta-tools.nl'
, 'xxvt001'
, 'Register duration of loading exchange rates into database.'
, sys_context('USERENV', 'SQL_DURATION_SEC', 'valutatools')
, 'Seconds'
)

insert into auditevents@DataDictionary
( is_monitoring
, metric_name
, service_name
, message_code
, user_message
, metric_value
, unit_of_measure
)
values
( true
, 'LoadDatabaseExchangeRatesErrors'
, 'valuta-tools.nl'
, 'xxvt002'
, 'Register outcome of job.'
, to_number('${stat:errorcountcontinue}')
, 'None'
)