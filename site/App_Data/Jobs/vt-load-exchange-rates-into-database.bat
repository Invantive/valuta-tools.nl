rem
rem Actual load of exchange rates.
rem

set INVANTIVE_CONFIGURATION_FOLDER=d:\jobs\cfg

set INVANTIVE_DB=VALUTATOOLS\backend

set INVANTIVE_SQL_FILE=d:\jobs\scripts\vt-load-exchange-rates-into-database.sql

set INVANTIVE_LOG_FILE=d:\jobs\log\vt-load-exchange-rates-into-database\%%Y%%m.log

set INVANTIVE_LOG_FILE_OVERWRITE=False

set INVANTIVE_INTERACTIVE=False

set INVANTIVE_PRG=C:\Program Files (x86)\Invantive Software BV\Invantive Data Hub 17.31.58\Invantive.Producer.QueryEngine.exe

set INVANTIVE_LICENSE_KEY=xxx

"%INVANTIVE_PRG%" /verbose /database:"%INVANTIVE_DB%" /file:"%INVANTIVE_SQL_FILE%" /logfile:"%INVANTIVE_LOG_FILE%" /logoverwrite:"%INVANTIVE_LOG_FILE_OVERWRITE%" /interactive:%INVANTIVE_INTERACTIVE% /licensekey:%INVANTIVE_LICENSE_KEY%

IF %ERRORLEVEL% NEQ 0 (
  echo Failed Data Hub job with exit code %ERRORLEVEL%.
  exit /b 1
)

echo Completed Data Hub job with exit code %ERRORLEVEL%.
