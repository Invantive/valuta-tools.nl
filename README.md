# Getting Started with Currency Tools

The currency tools app enables users to work with currencies such as exchange rates. It currently contains a tool for loading exchange rates into Exact Online, located at [valuta-tools.nl](https://valuta-tools.nl) and [currency-toolkit.com](https://currency-toolkit.com).

You can also install it yourself.

## Quickstart with IIS

For use with Microsoft IIS:

1. Ensure Microsoft IIS is running.
2. Install Invantive Data Access Point 17.31.176 or newer.
3. Put the code in the same folder as Invantive Data Access Point.
4. Configure app in Exact Online.
5. Configure client ID and redirect URL in the config folder settings-sample.xml. Review it carefully.
6. Rename settings-sample.xml as settings.xml.
7. Review the contents of application-sample.xml and change where deemed necessary.
8. Rename application-sample.xml to application.xml.
9. Navigate to the URL where you have Invantive Data Access Point running.

## Quickstart with Invantive Control for Excel

For use with Invantive Control:

1. Install Invantive Control for Excel on your PC.
2. Place settings-sample.xml as settings-valuta-tools-nl.xml in the Invantive settings folder.
3. Remove the clientid and api-redirect-url from the settings-valuta-tools-nl.xml. They will be inherited from Invantive Control.
4. Start Excel.
5. Log on to the new database such as 'ecbeolnl'.
6. Start the Invantive Query Tool.
7. Open the script `any-currency-import-query.sql` in the Templates folder.

Please report issues and feature requests through the issues.