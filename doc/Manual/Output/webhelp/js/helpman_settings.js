/* Project settings */
var hmAnimate = true;
var hmPopupSticky = true;
var hmImageLightbox = true;
var hmVideoLightbox = true;
var hmLightboxConstrained = true;
var hmForceRedirect = false;
var hmTocSingleClick = true;
var autocollapse = false;
var gaaccount = "UA-284774-4",
    gatrackername = "vt",
    gatracklevels = 9;
var initialtocstate = "collapsed";
