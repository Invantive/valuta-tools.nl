hmLoadTopic({
hmKeywords:"Browser,Currency,Exact Online,Exchange rate,iPhone,Tablet",
hmTitle:"Introduction",
hmDescription:"Currency toolkit is an online solution to provision Exact Online with exchange rates for over 200 currencies.",
hmPrevLink:"",
hmNextLink:"invantive-base-contact-information.html",
hmParentLink:"index.html",
hmBreadCrumbs:"",
hmTitlePath:"Introduction",
hmHeader:"<h1 class=\"p_Heading1\"><span class=\"f_Heading1\">Introduction<\/span><\/h1>\n\r",
hmBody:"<p class=\"p_Normal\"><a href=\"https:\/\/valuta-tools.nl\" target=\"_blank\" onclick=\"return HMTrackTopiclink(this);\" class=\"weblink\">Currency toolkit<\/a> is an online solution to provision Exact Online with exchange rates for over 200 currencies.<\/p>\n\r<p class=\"p_Normal\">Currency toolkit runs on most current web browsers and mobile devices such as tablets and iPhone.<\/p>\n\r"
})
