dictwords = ["currency 0 34 198 1 8 128",
	"toolkit 0 29 198 1 8 128",
	"online 0 32 70",
	"solution 0 15 68",
	"provision 0 15 68",
	"exact 0 21 68",
	"exchange 0 21 66",
	"rates 0 15 66",
	"over 0 15 66",
	"200 0 15 66",
	"currencies 0 15 66",
	"runs 0 10 2",
	"most 0 10 1",
	"current 0 10 1",
	"web 0 10 1 1 10 32",
	"browsers 0 10 1",
	"mobile 0 10 1",
	"devices 0 10 1",
	"such 0 10 1",
	"tablets 0 10 1",
	"iphone 0 16 65",
	"contents 0 10 1 1 10 1",
	"index 0 10 1 1 10 1",
	"search 0 10 1 1 10 1",
	"introduction 0 8 128",
	"index.html 0 6 64",
	"browser 0 7 64",
	"rate 0 7 64",
	"tablet 0 7 64",
	"invantive 1 44 98",
	"distributor 1 15 96",
	"software 1 39 98",
	"solutions 1 15 96",
	"owned 1 15 96",
	"location 1 10 32",
	"harderwijk 1 16 96",
	"biesteweg 1 10 32",
	"3849 1 10 32",
	"hierden 1 10 32",
	"netherlands 1 10 32",
	"phone 1 10 32",
	"500 1 10 32",
	"fax 1 10 32",
	"2258178 1 10 32",
	"e-mail 1 18 40",
	"info 1 10 32",
	"invantive.com 1 41 56",
	"https 1 26 48",
	"chamber 1 16 80",
	"commerce 1 16 80",
	"13031406 1 10 16",
	"managing 1 10 16",
	"director 1 10 16",
	"guido 1 10 16",
	"leenders 1 10 16",
	"company 1 10 16",
	"domiciled 1 10 16",
	"roermond 1 10 16",
	"bank 1 16 80",
	"rabo 1 18 16",
	"4097 1 10 16",
	"bic 1 10 16",
	"vat 1 16 80",
	"nl812602377b01 1 10 16",
	"founded 1 10 16",
	"1992 1 10 16",
	"2012 1 10 16",
	"naics 1 10 16",
	"511210 1 10 16",
	"support 1 25 80",
	"599 1 10 16",
	"forums 1 10 16",
	"forums.invantive.com 1 10 16",
	"customer 1 16 80",
	"portal 1 16 80",
	"cloud.invantive.com 1 10 16",
	"finance 1 18 16",
	"sales 1 18 8",
	"opening 1 32 76",
	"hours 1 32 76",
	"cet 1 20 8",
	"monday 1 20 8",
	"friday 1 20 8",
	"excluding 1 10 8",
	"dutch 1 10 8",
	"holidays 1 10 8",
	"privacy 1 16 72",
	"policy 1 24 74",
	"security 1 60 79",
	"incidents 1 27 12",
	"598 1 12 8",
	"email 1 25 76",
	"always 1 18 10",
	"include 1 10 8",
	"your 1 18 8",
	"telephone 1 10 8",
	"number 1 18 12",
	"address 1 10 8",
	"short 1 10 8",
	"description 1 10 8",
	"please 1 18 12",
	"not 1 33 7",
	"give 1 10 4",
	"sensitive 1 10 4",
	"details 1 10 4",
	"until 1 18 6",
	"secure 1 10 4",
	"communication 1 10 4",
	"channel 1 10 4",
	"been 1 10 4",
	"established 1 10 4",
	"urgent 1 10 4",
	"send 1 10 4",
	"both 1 10 4",
	"outside 1 10 4",
	"call 1 10 4",
	"display 1 10 4",
	"will 1 18 6",
	"called 1 10 4",
	"back 1 10 4",
	"soon 1 10 4",
	"possible 1 18 5",
	"use 1 33 7",
	"threat 1 10 4",
	"matrix 1 10 4",
	"ncsc 1 31 71",
	"classify 1 10 4",
	"reported 1 10 4",
	"incident 1 31 71",
	"responsible 1 10 4",
	"disclosure 1 10 4",
	"guideline 1 10 4",
	"basis 1 10 2",
	"our 1 10 2",
	"receive 1 10 2",
	"confirmation 1 10 2",
	"receipt 1 10 2",
	"within 1 10 2",
	"working 1 10 2",
	"day 1 10 2",
	"ask 1 26 3",
	"share 1 10 2",
	"information 1 22 194",
	"about 1 10 2",
	"others 1 10 2",
	"had 1 18 2",
	"sufficient 1 18 2",
	"opportunity 1 18 2",
	"resolve 1 10 2",
	"problem 1 18 3",
	"users 1 10 2",
	"possibly 1 10 2",
	"updated 1 10 2",
	"version 1 10 2",
	"further 1 10 1",
	"any 1 18 1",
	"knowledge 1 10 1",
	"omit 1 10 1",
	"actions 1 10 1",
	"made 1 10 1",
	"after 1 10 1",
	"existence 1 10 1",
	"satisfied 1 10 1",
	"handling 1 10 1",
	"would 1 10 1",
	"like 1 10 1",
	"contact 1 22 193",
	"published 1 10 1",
	"march 1 10 1",
	"2022 1 10 1",
	"invantive-base-contact-information.html 1 6 64",
	"route 1 7 64"];
skipwords = ["and",
	"or",
	"the",
	"it",
	"is",
	"an",
	"on",
	"we",
	"us",
	"to",
	"of",
	"has",
	"be",
	"all",
	"for",
	"in",
	"as",
	"so",
	"are",
	"that",
	"can",
	"you",
	"at",
	"its",
	"by",
	"have",
	"with",
	"into"];
var STR_FORM_SEARCHFOR = "Search for:";
var STR_FORM_SUBMIT_BUTTON = "Submit";
var STR_FORM_RESULTS_PER_PAGE = "Results per page:";
var STR_FORM_MATCH = "Match:";
var STR_FORM_ANY_SEARCH_WORDS = "any words";
var STR_FORM_ALL_SEARCH_WORDS = "all words";
var STR_NO_QUERY = "No search query entered.";
var STR_RESULTS_FOR = "Search results for:";
var STR_NO_RESULTS = "No results";
var STR_RESULT = "result";
var STR_RESULTS = "results";
var STR_PHRASE_CONTAINS_COMMON_WORDS = "Your search query contained too many common words to return the entire set of results available. Please try again with a more specific query for better results.";
var STR_SKIPPED_FOLLOWING_WORDS = "The following word(s) are in the skip word list and have been omitted from your search:";
var STR_SKIPPED_PHRASE = "Note that you can not search for exact phrases beginning with a skipped word";
var STR_SUMMARY_NO_RESULTS_FOUND = "No results found.";
var STR_SUMMARY_FOUND_CONTAINING_ALL_TERMS = "found containing all search terms.";
var STR_SUMMARY_FOUND_CONTAINING_SOME_TERMS = "found containing some search terms.";
var STR_SUMMARY_FOUND = "found.";
var STR_PAGES_OF_RESULTS = "pages of results.";
var STR_POSSIBLY_GET_MORE_RESULTS = "You can possibly get more results searching for";
var STR_ANY_OF_TERMS = "any of the terms";
var STR_DIDYOUMEAN = "Did you mean:";
var STR_SORTEDBY_RELEVANCE = "Sorted by relevance";
var STR_SORTBY_RELEVANCE = "Sort by relevance";
var STR_SORTBY_DATE = "Sort by date";
var STR_SORTEDBY_DATE = "Sorted by date &#9660;";
var STR_RESULT_TERMS_MATCHED = "Terms matched: ";
var STR_RESULT_SCORE = "Score: ";
var STR_RESULT_URL = "URL:";
var STR_RESULT_PAGES = "Result Pages:";
var STR_RESULT_PAGES_PREVIOUS = "Previous";
var STR_RESULT_PAGES_NEXT = "Next";
var STR_FORM_CATEGORY = "Category:";
var STR_FORM_CATEGORY_ALL = "All";
var STR_FORM_DATE_FROM = "From:";
var STR_FORM_DATE_TO = "To:";
var STR_FORM_DATE_BUTTON = "&laquo;";
var STR_RESULTS_IN_ALL_CATEGORIES = "in all categories";
var STR_RESULTS_IN_CATEGORY = "in category";
var STR_POWEREDBY = "Search powered by";
var STR_MORETHAN = "More than";
var STR_ALL_CATS = "all categories";
var STR_CAT_SUMMARY = "Refine your search by category:";
var STR_OR = "or";
var STR_RECOMMENDED = "Recommended links";
var STR_SORTEDBY_DATE_ASC = "Sorted by date &#9650;";
var STR_SEARCH_TOOK = "Search took";
var STR_SECONDS = "seconds";
var STR_MAX_RESULTS = "You have requested more results than served per query. Please try again with a more precise query.";
