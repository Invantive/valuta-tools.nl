﻿using Invantive.Deploy.CryptoObfuscater;
using Invantive.Deploy.Specification;
using System.IO;

namespace Invantive.Deploy.Setup
{
    class Program
    {
        static void Main()
        {
            //
            // Make changes only to product-version.js.
            //
            // No changes below.
            //
            ProductVersion productVersion = ProductVersion.LoadFromFile();

            string solutionFolder = FolderLocations.GetSolutionFolder("Valuta Tools");

            Specification.Product product = Product.LoadFromFile(Path.Combine(solutionFolder, "product-orig.json"));

            product.ProductVersion = productVersion;
            product.SolutionFolder = solutionFolder;
            product.ObfuscationNameSpace = "VTL";
            product.ApplicationProjectFolder = Path.Combine(FolderLocations.GetSourceFolder(), "Invantive.ValutaTools");
            product.ObfuscationSetting = PredefinedObfuscationSettings.ObfuscationSettingsDefault;

            InvantiveValutaToolsProject project = new InvantiveValutaToolsProject(product);

            project.Complete();

            project.Build();

            product.SaveToFile();
        }
    }
}
