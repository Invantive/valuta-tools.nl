using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c9a243e5-6443-4a00-acac-c20a61286264")]

[assembly: AssemblyInformationalVersion("22.0.32-PROD+1716")]
[assembly: AssemblyVersion("22.0.32.0")]
[assembly: AssemblyFileVersion("22.0.32-PROD+1716")]
// Auto-generated lines:
[assembly: AssemblyCompany("Invantive Software B.V.")]
[assembly: AssemblyTitle("Valuta Tools")]
[assembly: AssemblyProduct("Valuta Tools")]
[assembly: AssemblyTrademark("Invantive")]
[assembly: AssemblyCopyright("(C) Copyright 2004-2022 Invantive Software B.V., the Netherlands. All rights reserved.")]
[assembly: AssemblyDescription("Valuta Tools offers import of exchange rates from ECB and Open Exchange Rates into Exact Online companies.")]
