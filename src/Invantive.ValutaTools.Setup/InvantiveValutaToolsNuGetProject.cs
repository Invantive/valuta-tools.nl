﻿namespace Invantive.Deploy.Setup
{
    /// <summary>
    /// Update release notes of Valuta Tools.
    /// </summary>
    class InvantiveValutaToolsProject : NoCodeProject
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="product">Product.</param>
        public InvantiveValutaToolsProject(Specification.Product product)
            : base(product)
        {
            this.Product.EnsureSoftwareRequirementListed("Invantive Data Access Point 22.0.25");
            this.Product.EnsureSoftwareRequirementListed(KnownMicrosoftNetReleases.Net472OrHigher);
            this.Product.EnsureSupportedOsListed(product.GetOperatingSystemsForWindowsWebApplications());
            this.Product.PublishReleaseNotes = true;

            //
            // Valuta Tools is managed on gitlab.com.
            // 
            // Manually add all release notes to releasenotes.xml.
            //
            this.Product.PushGitTags = false;
            this.Product.UpdateReleaseNotes = false;
            this.Product.TweetRelease = true;
            this.Product.TweetBetaRelease = false;
        }
    }
}
